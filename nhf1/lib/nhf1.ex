defmodule Nhf1 do
  @moduledoc """
  Kemping
  @author "Szommer Zsombor <zsombor.szommer@protonmail.com>"
  @date   "2022-10-12"
  ...
  """

  # sor száma (1-től n-ig)
  @type row :: integer
  # oszlop száma (1-től m-ig)
  @type col :: integer
  # egy parcella koordinátái
  @type field :: {row, col}

  # a sátrak száma soronként
  @type tents_count_rows :: [integer]
  # a sátrak száma oszloponként
  @type tents_count_cols :: [integer]

  # a fákat tartalmazó parcellák koordinátái lexikálisan rendezve
  @type trees :: [field]
  # a feladványleíró hármas
  @type puzzle_desc :: {tents_count_rows, tents_count_cols, trees}

  # a sátorpozíciók iránya: north, east, south, west
  @type dir :: :n | :e | :s | :w
  # a sátorpozíciók irányának listája a fákhoz képest
  @type tent_dirs :: [dir]

  @type tree_set :: MapSet.t()

  @type tent_map :: %{field => dir}

  @spec pre_check_adjacent_fields(tent_map :: tent_map, pos :: field) :: boolean
  defp pre_check_adjacent_fields(tent_map, {row, col}) do
    has_adjacent =
      tent_map |> Map.has_key?({row - 1, col - 1}) or
        tent_map |> Map.has_key?({row - 1, col}) or
        tent_map |> Map.has_key?({row - 1, col + 1}) or
        tent_map |> Map.has_key?({row, col + 1}) or
        tent_map |> Map.has_key?({row + 1, col + 1}) or
        tent_map |> Map.has_key?({row + 1, col}) or
        tent_map |> Map.has_key?({row + 1, col - 1}) or
        tent_map |> Map.has_key?({row, col - 1})

    not has_adjacent
  end

  @spec tent_count_row(tent_map :: tent_map, row :: integer) :: non_neg_integer
  defp tent_count_row(tent_map, row) do
    tent_map
    |> Map.keys()
    |> Enum.filter(fn {key_row, _} -> key_row === row end)
    |> length()
  end

  @spec pre_check_tent_count_row(
          expected_tent_count_row :: integer,
          tent_map :: tent_map,
          row :: integer
        ) :: boolean
  defp pre_check_tent_count_row(expected_tent_count_row, _, _) when expected_tent_count_row < 0,
    do: true

  defp pre_check_tent_count_row(expected_tent_count_row, map, row),
    do: tent_count_row(map, row) <= expected_tent_count_row

  @spec tent_count_col(tent_map :: tent_map, col :: integer) :: non_neg_integer
  defp tent_count_col(tent_map, col) do
    tent_map
    |> Map.keys()
    |> Enum.filter(fn {_, key_col} -> key_col === col end)
    |> length()
  end

  @spec pre_check_tent_count_col(
          expected_tent_count_row :: integer,
          tent_map :: tent_map,
          col :: integer
        ) :: boolean
  defp pre_check_tent_count_col(expected_tent_count_col, _, _) when expected_tent_count_col < 0,
    do: true

  defp pre_check_tent_count_col(expected_tent_count_col, map, col),
    do: tent_count_col(map, col) <= expected_tent_count_col

  @spec pre_check(
          tents_count_rows :: tents_count_rows,
          tents_count_cols :: tents_count_cols,
          tent_map :: tent_map,
          pos :: field
        ) :: boolean
  defp pre_check(tents_count_rows, tents_count_cols, _, {row, col})
       when row < 1 or col < 1 or row > length(tents_count_rows) or col > length(tents_count_cols),
       do: false

  defp pre_check(tents_count_rows, tents_count_cols, tent_map, {row, col}) do
    pre_check_adjacent_fields(tent_map, {row, col}) and
      pre_check_tent_count_row(tents_count_rows |> Enum.at(row - 1), tent_map, row) and
      pre_check_tent_count_col(tents_count_cols |> Enum.at(col - 1), tent_map, col)
  end

  @spec dir_and_tree_to_pos(dir :: dir, pos :: field) :: field
  defp dir_and_tree_to_pos(dir, {row, col}) do
    case dir do
      :n -> {row - 1, col}
      :e -> {row, col + 1}
      :s -> {row + 1, col}
      :w -> {row, col - 1}
    end
  end

  @spec check_tents_count_rows(
          tents_count_rows :: tents_count_rows,
          tent_map :: tent_map,
          row :: integer
        ) :: boolean
  defp check_tents_count_rows([], _, _), do: true

  defp check_tents_count_rows([tents_count_row | tents_count_rows], tent_map, row)
       when tents_count_row < 0,
       do: check_tents_count_rows(tents_count_rows, tent_map, row + 1)

  defp check_tents_count_rows([tents_count_row | tents_count_rows], tent_map, row) do
    tent_count_row(tent_map, row) === tents_count_row and
      check_tents_count_rows(tents_count_rows, tent_map, row + 1)
  end

  @spec check_tents_count_cols(
          tents_count_cols :: tents_count_cols,
          tent_map :: tent_map,
          row :: integer
        ) :: boolean
  defp check_tents_count_cols([], _, _), do: true

  defp check_tents_count_cols([tents_count_col | tents_count_cols], tent_map, col)
       when tents_count_col < 0,
       do: check_tents_count_cols(tents_count_cols, tent_map, col + 1)

  defp check_tents_count_cols([tents_count_col | tents_count_cols], tent_map, col) do
    tent_count_col(tent_map, col) === tents_count_col and
      check_tents_count_cols(tents_count_cols, tent_map, col + 1)
  end

  @spec solve(
          tents_count_rows :: tents_count_rows,
          tents_count_cols :: tents_count_cols,
          tent_map :: tent_map,
          tree_set :: tree_set,
          trees :: trees,
          [tent_dirs]
        ) :: [tent_dirs]
  defp solve(tents_count_rows, tents_count_cols, tent_map, tree_set, [tree | trees], acc) do
    [:n, :e, :s, :w]
    |> List.foldl(acc, fn dir, acc2 ->
      pos = dir_and_tree_to_pos(dir, tree)

      if tent_map |> Map.has_key?(pos) or tree_set |> MapSet.member?(pos) do
        acc2
      else
        new_tent_map = tent_map |> Map.put(pos, dir)

        if pre_check(tents_count_rows, tents_count_cols, new_tent_map, pos) do
          solve(tents_count_rows, tents_count_cols, new_tent_map, tree_set, trees, acc2)
        else
          acc2
        end
      end
    end)
  end

  defp solve(tents_count_rows, tents_count_cols, tent_map, _, [], acc) do
    if check_tents_count_rows(tents_count_rows, tent_map, 1) and
         check_tents_count_cols(tents_count_cols, tent_map, 1) do
      solution =
        tent_map
        |> Map.to_list()
        |> List.foldl([], fn {{row, col}, char}, ds ->
          case char do
            :n -> [{{row + 1, col}, :n} | ds]
            :e -> [{{row, col - 1}, :e} | ds]
            :s -> [{{row - 1, col}, :s} | ds]
            :w -> [{{row, col + 1}, :w} | ds]
            _ -> ds
          end
        end)
        |> Enum.sort(fn
          {{row, col1}, _}, {{row, col2}, _} -> col1 <= col2
          {{row1, _}, _}, {{row2, _}, _} -> row1 <= row2
        end)
        |> Enum.map(fn {_, d} -> d end)

      [solution | acc]
    else
      acc
    end
  end

  @spec satrak(pd :: puzzle_desc) :: tss :: [tent_dirs]
  # tss a pd feladványleíróval megadott feladvány összes megoldásának listája, tetszőleges sorrendben
  def satrak({tents_count_rows, tents_count_cols, trees}) do
    solve(tents_count_rows, tents_count_cols, Map.new(), MapSet.new(trees), trees, [])
  end
end
