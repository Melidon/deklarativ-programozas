defmodule Mintazh1Test do
  use ExUnit.Case
  doctest Mintazh1

  test "fel1 test" do
    Mintazh1.fel1()
  end

  test "szigetek test" do
    assert Mintazh1.szigetek([]) === []
    assert Mintazh1.szigetek([-1, 0, -2, -3, 0, 0]) === []
    assert Mintazh1.szigetek([1, 2, 3, 0, 0, 4, 5, 6]) === [[1, 2, 3], [4, 5, 6]]

    assert Mintazh1.szigetek([2, 1, 3, 0, 0, 7, 6, -1, -3, 0, 7, 5, 6]) === [
             [2, 1, 3],
             [7, 6],
             [7, 5, 6]
           ]
  end

  test "szfold test" do
    assert Mintazh1.szfold([0, 1, 0, 3, 4, 0, 0, 1]) === [{1, 1}, {2, 4}, {1, 1}]
    assert Mintazh1.szfold([0, 0, 30, 4, 10, 0, 0, 100]) === [{3, 30}, {1, 100}]
    assert Mintazh1.szfold([0, 0, 0]) === []
  end

  test "dec2hex test" do
    # assert Mintazh1.dec2hex(0) === '0'
    assert Mintazh1.dec2hex(7) === '7'
    assert Mintazh1.dec2hex(14) === 'E'
    assert Mintazh1.dec2hex(75) === '4B'
  end

  test "d2hfa test" do
    assert Mintazh1.d2hfa(:e) === :e
    assert Mintazh1.d2hfa({:n, 14, :e, :e}) === {:n, 'E', :e, :e}

    assert Mintazh1.d2hfa({:n, 75, {:n, 200, :e, :e}, {:n, 35, :e, :e}}) ===
             {:n, '4B', {:n, 'C8', :e, :e}, {:n, '23', :e, :e}}
  end
end
