# Követelmények elemzése

Ugyanaz a feladat mint ami az előző nagyházinál is volt, nics mit elemezni pluszban.

# Tervezés, az egyes eljárások, ill. függvények specifikálása

Célom volt, hogy felhasználjam a korábbi házifeladatok során elkészített algoritmusokat és azon kívül a lehető legkevesebb kódot kelljen írnom.

- A `minden_sator_szabaly`, `minden_sor_szabaly` és `minden_oszlop_szabaly` függvények annyit tudnak, hogy előállítják az összes lehetséges alkalmazható sátor, sort illetve oszlop szabályt. Paramétereik is nagyon hasonlóak. Az első egy számláló ami folyamatosan nő ahogy egyre mélyebbre jutunk a rekurzióban. A második az egyre fogyó lista ami alapján a szabályokat előállítják (fák, sorösszegek, oszlopösszegek). A harmadik a kimenő.
- A `minden_szabaly` az előző három függvény felhasználásával előállítja az összes lehetséges szabályt. Első három bemenő paramétere a sorösszegek, oszlopösszegek és fák. A negyedik a kimenő paraméter, amiben összefűzve megkapjuk az összes alkalmazható szabályt.
- Az `alkalmaz` függény feladata egy konkrét szabályt alkalmazni. Egy a korábbi kisházikban megír `sator_szukites` illetve `osszeg_szukites` egyikét hívja.
- A `mindet_szukit` feladata minden szabály alkalmazása egyszer. Végigmegy az összes szabályon és alkalmazza az összeset.
- A `szukit_amig_tud` feladata, hogy addig hívogasa az előző függvényt amíg tudja. Az előző fügvényt hívogatja amíg csak tudja. Ha közben az iránylisták üres lenne akkor elhal az a megoldási ág. Amúgy visszaadja az addig leszűkített iránylistákat.
- A `valaszt` függvény feladata, hogy az iránylisták első olyan iránylistájára amelyik nem csak egy elemet tartalmaz, hív egy `member` függvényt ezáltal egy prologos elágazást létrehozva.
- A `flatten_ILs` kilapít egy listát. Érdekessége, hogy csak olyan listát tud kilapítani aminek az elemei pontosen egyelemű listák.
- A `megold` függvény szűkít amíg tud. Ha az eredményt ki tudja lapítani akkor készen vagyunk, ezen az ágon született egy megoldás. Amennyiben nem, akkor választás segítségével elágazik egyet, majd kezdi előről a szűkítéssel.
- A `satrak` függvény meghívja a korábbi kisháziban megírt iránylisták függvényt, előállítja az összes szabály listáját. Ezután minden paraméter rendelkezésre áll, hogy meghívja a megold függvényt, úgyhogy így is tesz.

# Kódolás, az algoritmusok ismertetése

A kódolás egy szenvedés volt. Nagyon remélem, hogy cserébe nem kell megírnom a második ZH mert egy életre elég volt a prolog.

Az algoritmus elve a következő. Készítek egy listát az alkalmazható szabályokból, majd alkalmazom őket addig amíg csak lehet. Amennyiben már egyik szabály sem alkalmazható, akkor elágaztatom a kódot prologos módon, azaz a hívok egy `member` függvényet az iránylisták első olyan iránylistájára amelyik nem csak egy elemet tartalmaz (mármint egy olyan iránylistára cserélem le amiben csak ez az egy elem található meg). Ezután kezdem előröl az egészet. Próbálom alkalmazni azokat a szabályokat amiket még nem sikerült (hiszen a választás után már lehet, hogy fog), választok ha megakatdam, stb.

# Tesztelési megfontolások

Nincsen SICStus prologom és nem akarok a csomagkezelőmön kívülről telepíteni alkalmazásokat a gépemre, a licenszszel meg még annyira sem akarok bajlódni.
Nagyon kedves, hogy kaptunk egy külön összeállított ZIP-et a házihoz, biztosan sok munka volt, de hasznát nem nagyon vettem.
Úgyhogy a tesztelésem annyi, hogy írom csak simán [SWISH](https://swish.swi-prolog.org/)-ben, ott tesztelek egyet kézzel, ha átment, akkor beadom a kódomat.
Amennyiben hiba van, akkor az első hibás teszt alapján javítgatok amíg nem jó és beadom újra. Véges számú ismétlés után kész a megoldás.

# Kipróbálási tapasztalatok

Nem tudom mit kéne írni ehhez a ponthoz. Működtek a tesztek. Amúgy érdekes volt, hogy általában lassabban futott a kód mint amit az állítólagos mintamegoldás tud, viszont volt egy-két teszt ahol meg pont gyorsabb volt. Ötletem sincsen, hogy ez miért lehet.
