defmodule Gyak2Test do
  use ExUnit.Case

  test "osszetett test" do
    assert Gyak2.osszetett(5) === [4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25]
  end

  test "primek test" do
    assert Gyak2.primek(5) === [2, 3, 5, 7, 11, 13, 17, 19, 23]
  end

  test "duplak test" do
    assert Gyak2.duplak([1, 2, 3]) === []
    assert Gyak2.duplak([1, 1, 2, 3, 3, 3]) === [1, 3, 3]
  end

  test "all_different test" do
    assert Gyak2.all_different([1, 2, 3, 1]) === false
    assert Gyak2.all_different([1, 2, 3]) === true
  end

  test "all test" do
    assert Gyak2.all(&is_atom/1, [:a, :b]) === true
    assert Gyak2.all(&is_atom/1, [:a, 1]) === false
  end

  test "platok_hossza test" do
    assert Gyak2.platok_hossza([:a, :a, :a, :b, :b, :d, :f, :f, :h]) === [
             {:a, 3},
             {:b, 2},
             {:d, 1},
             {:f, 2},
             {:h, 1}
           ]
  end

  test "negyzetes test" do
    assert Gyak2.negyzetes(2) === [[1, 5, 9, 13], [2, 6, 10, 14], [3, 7, 11, 15], [4, 8, 12, 16]]
    assert Gyak2.matrix_ki(Gyak2.negyzetes(2)) == :ok
  end

  test "kozepe test" do
    assert Gyak2.kozepe([[:a, :b, :e, :f], [:c, :d, :g, :h], [:i, :j, :m, :n], [:k, :l, :o, :p]]) ===
             [[:d, :g], [:j, :m]]
  end

  test "laposkozepe test" do
    assert Gyak2.laposkozepe([
             [:a, :b, :e, :f],
             [:c, :d, :g, :h],
             [:i, :j, :m, :n],
             [:k, :l, :o, :p]
           ]) ===
             [:d, :g, :j, :m]
  end

  test "pivot test" do
    assert Gyak2.pivot([[:a,:b,:e,:f], [:c,:d,:g,:h], [:i,:j,:m,:n], [:k,:l,:o,:p]], 1, 2) === [[:a,:b,:f], [:i,:j,:n],[:k,:l,:p]]
  end

  test "pivot transpose" do
    assert Gyak2.transpose([[:a,:b], [:c,:d], [:e,:f]]) === [[:a,:c,:e], [:b,:d,:f]]
  end
end
