defmodule Khf3Test do
  use ExUnit.Case
  doctest Khf3

  test "check_sol test 0" do
    assert Khf3.check_sol({[-1, 0, 0, -3, 0], [0, 0, -2, 0, 0], []}, []) ===
             {%{err_rows: []}, %{err_cols: []}, %{err_touch: []}}
  end

  test "check_sol test 1" do
    assert Khf3.check_sol({[1, 0, 0, 3, 0], [0, 0, 2, 0, 0], []}, []) ===
             {%{err_rows: [1, 4]}, %{err_cols: [3]}, %{err_touch: []}}
  end

  test "check_sol test 2" do
    assert Khf3.check_sol(
             {[1, 1, 0, 3, 0], [1, 0, 2, 0, 2], [{1, 2}, {3, 3}, {3, 5}, {5, 1}, {5, 5}]},
             [:e, :s, :n, :n, :n]
           ) === {%{err_rows: []}, %{err_cols: []}, %{err_touch: []}}
  end

  test "check_sol test 3" do
    assert Khf3.check_sol(
             {[1, 1, 0, 3, 0], [1, 0, 2, 0, 2], [{1, 2}, {3, 3}, {3, 5}, {5, 1}, {5, 5}]},
             [:e, :e, :n, :n, :n]
           ) ===
             {%{err_rows: [3, 4]}, %{err_cols: [3, 4]}, %{err_touch: [{2, 5}, {3, 4}, {4, 5}]}}
  end

  test "check_sol test 4" do
    assert Khf3.check_sol(
             {[1, 0, 2, 2, 0], [1, 0, 0, 2, 1], [{1, 2}, {3, 3}, {3, 5}, {5, 1}, {5, 5}]},
             [:e, :e, :n, :n, :n]
           ) ===
             {%{err_rows: [2, 3]}, %{err_cols: [3, 4, 5]}, %{err_touch: [{2, 5}, {3, 4}, {4, 5}]}}
  end

  test "check_sol test 5" do
    assert Khf3.check_sol(
             {[1, 1, -1, 3, 0], [1, 0, -2, 0, 2], [{1, 2}, {3, 3}, {3, 5}, {5, 1}, {5, 5}]},
             [:e, :s, :n, :n, :w]
           ) === {%{err_rows: [4, 5]}, %{err_cols: [4, 5]}, %{err_touch: [{4, 3}, {5, 4}]}}
  end
end
