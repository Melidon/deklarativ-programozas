defmodule Gyak1.Lnko do
  @spec lnko(a :: integer, b :: integer) :: integer
  # a és b legnagyobb közös osztója d
  def lnko(a, 0) do
    a
  end

  def lnko(a, b) do
    lnko(b, rem(a, b))
  end
end

defmodule Gyak1.Len do
  @spec len(xs :: [any]) :: integer
  # Az xs lista hossza n
  def len([]) do
    0
  end

  def len([_ | xs]) do
    1 + len(xs)
  end

  @spec leni(xs :: [any]) :: integer
  # Az xs lista hossza n
  @spec leni(xs :: [any], acc :: integer) :: integer
  # az xs lista hossza + acc = n

  def leni(xs) do
    leni(xs, 0)
  end

  def leni([], acc) do
    acc
  end

  def leni(xs, acc) do
    leni(tl(xs), acc + 1)
  end
end

defmodule Gyak1.Pi do
  @spec pi(i :: integer) :: float
  # A pi i-edik közelítő értéke pi

  def pi(i) do
    pi(i, :math.pow(-1, i - 1))
  end

  def pi(1, 1) do
    4.0
  end

  def pi(i, sign) do
    pi(i - 1, -sign) + sign * 4 / (2 * i - 1)
  end
end

defmodule Gyak1.Dec2rad do
  @spec dec2rad(r :: integer, i :: integer) :: [integer]
  # Az i egész szám r alapú számrendszerbe konvertált, decimális számként
  # megadott számjegyeinek listája ds

  def dec2rad(r, i) do
    dec2rad(r, i, [])
  end

  def dec2rad(_, 0, acc) do
    acc
  end

  def dec2rad(r, i, acc) do
    dec2rad(r, div(i, r), [rem(i, r) | acc])
  end
end

defmodule Gyak1.Last do
  @spec last_er(xs :: [any]) :: {:ok, x :: any} | :error
  # Ha xs üres, r == :error, különben {:ok, x}, ahol x az xs utolsó elemex

  def last_er([]) do
    :error
  end

  def last_er([x | []]) do
    {:ok, x}
  end

  def last_er([_ | xs]) do
    last_er(xs)
  end

  @spec last_ex(xs :: [any]) :: (x :: any) | nil
  # Ha xs üres, r == nil, különben x, ahol x az xs utolsó eleme

  def last_ex([]) do
    nil
  end

  def last_ex([x | []]) do
    x
  end

  def last_ex([_ | xs]) do
    last_ex(xs)
  end
end

defmodule Gyak1.At do
  @spec at(es :: [any], n :: integer) :: (e :: any) | nil
  # Az es lista n-edik eleme e (indexelés 0-tól)

  def at([], _) do
    nil
  end

  def at([x | _], 0) do
    x
  end

  def at([_ | xs], n) do
    at(xs, n - 1)
  end
end

defmodule Gyak1.Split do
  @spec split(ls :: [any], n :: integer) :: {[any], [any]}
  # Az ls lista n hosszú prefixuma (első n eleme) ps, length(ls) - n
  # hosszú szuffixuma (első n eleme utáni része) pedig ss

  def split([], _) do
    {[], []}
  end

  def split(xs, 0) do
    {[], xs}
  end

  def split([x | xs], n) do
    {ps, ss} = split(xs, n - 1)
    {[x | ps], ss}
  end
end

defmodule Gyak1.Take do
  @spec take(xs :: [any], n :: integer) :: [any]
  # Az xs lista n hosszú prefixuma a ps lista

  def take([], _) do
    []
  end

  def take(_, 0) do
    []
  end

  def take([x | xs], n) do
    [x | take(xs, n - 1)]
  end
end

defmodule Gyak1.Drop do
  @spec drop(xs :: [any], n :: integer) :: ss :: [any]
  # Az xs lista első n elemét nem tartalmazó szuffixuma ss

  def drop([], _) do
    []
  end

  def drop(xs, 0) do
    xs
  end

  def drop([_ | xs], n) do
    drop(xs, n - 1)
  end
end

defmodule Gyak1.Tails do
  @spec tails(xs :: [any]) :: [[any]]
  # Az xs lista egyre rövidülő szuffixumainak listája zss

  def tails([]) do
    [[]]
  end

  def tails([x | xs]) do
    [[x | xs] | tails(xs)]
  end
end

defmodule Gyak1.Parban do
  @spec parban(es :: [any]) :: zs :: [any]
  # Az es lista összes olyan elemének listája zs, amely
  # után vele azonos értékű elem áll

  def parban([_ | []]) do
    []
  end

  def parban([x1 | [x2 | xs]]) do
    if x1 === x2 do
      [x1 | parban([x2 | xs])]
    else
      parban([x2 | xs])
    end
  end
end

defmodule Gyak1.Vertekek do
  @spec vertekek(xs :: [any]) :: es :: [any]
  # Az xs lista elemei közül a {v::atom, e:any} mintára illeszkedő
  # párok 2. tagjából képzett lista es

  def vertekek([]) do
    []
  end

  def vertekek([{a, b} | xs]) do
    if(a === :v) do
      [b | vertekek(xs)]
    else
      vertekek(xs)
    end
  end

  def vertekek([_ | xs]) do
    vertekek(xs)
  end
end

defmodule Gyak1.Dadogo do
  @spec dadogo(xs :: [any]) :: zss :: [[any]]
  # zss az xs lista összes olyan nemüres (folytonos) részlistájából
  # álló lista, amelyet vele azonos értékű részlista követ

  def dadogo_2(xs, l) do
    {first_l, remaining} = Gyak1.Split.split(xs, l)

    if remaining === [] do
      []
    else
      second_l = Gyak1.Take.take(remaining, l)

      if first_l === second_l do
        [first_l | dadogo_2(xs, l + 1)]
      else
        dadogo_2(xs, l + 1)
      end
    end
  end

  def dadogo_1([]) do
    []
  end

  def dadogo_1([x | xs]) do
    result = dadogo_2(x, 1)

    if(result === []) do
      dadogo_1(xs)
    else
      result ++ dadogo_1(xs)
    end
  end

  def dadogo([]) do
    []
  end

  def dadogo(xs) do
    t = Gyak1.Tails.tails(xs)
    dadogo_1(t)
  end
end
