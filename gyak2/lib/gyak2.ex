defmodule Gyak2 do
  @spec osszetett(k :: integer) :: xs :: [integer]
  # A 4..k*k közötti összetett számok növekvő listája, ismétlődés nélkül

  def osszetett(k) do
    szamok = for i <- 2..k, j <- 2..div(k * k, i), do: i * j
    :lists.usort(szamok)
  end

  @spec primek(k :: integer) :: xs :: [integer]
  # A 2..k*k közötti prímszámok listája xs

  def primek(k) do
    for i <- 2..(k * k), i not in osszetett(k), do: i
  end

  @spec duplak(xs :: [any]) :: ys :: [any]
  # ys az xs azon elemeinek listája, melyek egyenlők az őket követő elemmel

  def duplak(xs) do
    parok = Enum.zip(xs, tl(xs))
    for {x, y} <- parok, x == y, do: x
  end

  @spec all_different(xs :: [any]) :: b :: boolean
  # b igaz, ha az xs listában csupa különböző értékű elem van

  def all_different(xs) do
    duplak(Enum.sort(xs)) === []
  end

  @spec all(p :: (t :: any -> boolean), xs :: [t :: any]) :: b :: boolean
  # b akkor és csak akkor igaz, ha p teljesül xs minden elemére

  def all(p, xs) do
    mapped = Enum.map(xs, p)
    List.foldl(mapped, true, fn x, acc -> x and acc end)
  end

  @spec platok_hossza(xs :: [any]) :: phs :: [{p :: any, h :: integer}]
  # phs olyan {p, h} párok listája, amelyekben p a platót képező
  # érték, h pedig e plató hossza (= azonos értékű elemeinek száma)

  defp platok_hossza_help(x, [{x, c} | xs]), do: [{x, c + 1} | xs]
  defp platok_hossza_help(x, acc), do: [{x, 1} | acc]

  def platok_hossza(xs), do: List.foldr(xs, [], &platok_hossza_help/2)

  @type mx :: rs :: [cs :: [integer]]
  @spec negyzetes(k :: integer) :: mss :: mx
  # k^2 méretű mátrixok olyan k^4 méretű mátrixa mss, amelyben
  # az elemek értéke 1-től k^4-ig minden sorban felülről lefelé
  # haladva egyesével nő: a bal felső (1,1) indexű elem értéke 1,
  # alatta az (1,0) indexűé 2 stb., a (k^2,k^k) indexűé pedig k^4
  @spec matrix_ki(mss :: mx) :: :ok
  # a negyzetes/1 függvény által előállított mátrix kiírása a
  # képernyőre úgy, hogy a mátrix minden sora új sorba kerüljön,
  # a számok pedig oszloponként balra legyenek illesztve, a
  # vezető 0-k helyén szóközzel

  def negyzetes(k) do
    kk = k * k
    for i <- 1..kk, do: for(j <- 1..kk, do: i + (j - 1) * kk)
  end

  def matrix_ki(mss) do
    item_concat = fn item, acc -> acc <> " " <> String.pad_leading(Integer.to_string(item), 2) end
    rows = for row <- mss, do: List.foldl(row, "", item_concat)
    row_concat = fn row, acc -> acc <> row <> "\n" end
    matrix = List.foldl(rows, "", row_concat)
    IO.write(matrix)
    :ok
  end

  @spec kozepe(mss :: [[any]]) :: rss :: [[any]]
  # rss az mss n*n-es négyzetes mátrix olyan (n/2)*(n/2) méretű részmátrixa,
  # mely az n/4+1. sor n/4+1. oszlopának elemétől kezdődik
  # négyzetes az olyan mátrix, amelynek n sora és n oszlopa van, ahol
  # az n>=4 négyzetszám

  def kozepe(mss) do
    n = length(mss)
    q = div(n, 4)
    range = q..(q + div(n, 2) - 1)
    for row <- Enum.slice(mss, range), do: Enum.slice(row, range)
  end

  @spec laposkozepe(mss :: [[any]]) :: xs :: [any]
  # Az xs lista az mss mátrix középső elemeinek listája
  def laposkozepe(mss), do: List.flatten(kozepe(mss))

  @spec pivot(mss :: [[any]], r :: integer, c :: integer) :: rss :: [[any]]
  # rss az mss mátrix r-edik sorának és c-edik oszlopának elhagyásával áll elő

  def pivot(mss, r, c) do
    range = 0..(length(mss) - 1)
    for i <- range, i != r, do: for(j <- range, j != c, do: Enum.at(Enum.at(mss, i), j))
  end

  @spec transpose(mss :: [[any]]) :: tss :: [[any]]
  # az mss valódi mátrix transzpontáltja tss

  def transpose(mss), do: Enum.zip_with(mss, fn ms -> ms end)
end
