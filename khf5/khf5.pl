:- use_module(library(lists)).

fabol_es_iranybol_sator(ROW-COL, e, ROW-COL1) :- COL1 is COL+1.
fabol_es_iranybol_sator(ROW-COL, n, ROW1-COL) :- ROW1 is ROW-1.
fabol_es_iranybol_sator(ROW-COL, s, ROW1-COL) :- ROW1 is ROW+1.
fabol_es_iranybol_sator(ROW-COL, w, ROW-COL1) :- COL1 is COL-1.

szabad_hely(N-M, [], ROW-COL) :-
    0 < ROW, ROW =< N,
    0 < COL, COL =< M.
szabad_hely(NM, [F|Fs], S) :-
    \+ F = S,
    szabad_hely(NM, Fs, S).

iranylistak_seged(_, _, [], []).
iranylistak_seged(NM, Fs, [F | Maradek], [IL | ILs]) :-
    findall(I, (fabol_es_iranybol_sator(F, I, S), szabad_hely(NM, Fs, S)), IL),
    iranylistak_seged(NM, Fs, Maradek, ILs).

iranylistak(NM, Fs, RILs) :-
    iranylistak_seged(NM, Fs, Fs, ILs),
    (   member([], ILs) ->  RILs = []
    ;   RILs = ILs
    ).

oldalszomszed(MINEK, MI) :- fabol_es_iranybol_sator(MINEK, _, MI).

sarokszomszed(ROW-COL, ROW1-COL1) :- ROW1 is ROW-1, COL1 is COL+1.
sarokszomszed(ROW-COL, ROW1-COL1) :- ROW1 is ROW+1, COL1 is COL+1.
sarokszomszed(ROW-COL, ROW1-COL1) :- ROW1 is ROW+1, COL1 is COL-1.
sarokszomszed(ROW-COL, ROW1-COL1) :- ROW1 is ROW-1, COL1 is COL-1.

szomszed(MINEK, MI) :-
    oldalszomszed(MINEK, MI) ; sarokszomszed(MINEK, MI).

szukit(_, [], [], []).
szukit(Nemszabadok, [Fa|Fak], [IL0|ILs0], [IL|ILs]) :-
    findall(
        Irany,
        (
            member(Irany, IL0),
            fabol_es_iranybol_sator(Fa, Irany, Sator),
            \+ member(Sator, Nemszabadok)
        ),
        IL
    ),
    szukit(Nemszabadok, Fak, ILs0, ILs).
    
sator_szukites(Fak, Index, ILs0, ILs) :-
    nth1(Index, ILs0, [Irany], MaradekIranyok),
    nth1(Index, Fak, Fa, MaradekFak),
    fabol_es_iranybol_sator(Fa, Irany, Sator),
    findall(Szomszed, szomszed(Sator, Szomszed), Szomszedok),
    Nemszabadok = [Sator | Szomszedok],
    szukit(Nemszabadok, MaradekFak, MaradekIranyok, SzukitettIranyok),
    nth1(Index, Eredmeny, [Irany], SzukitettIranyok),
    (   member([], Eredmeny) ->  ILs = []
    ;   ILs = Eredmeny
    ).
