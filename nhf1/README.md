# Első nagyházi dokumentáció

## Alap elgondolás

### Nem hatékony megoldás

Az első megoldási ötletem az volt, hogy előállítom a sátrak elhelyezkedésének összes lehetséges permutációját, majd mindegyiket leellenőrzöm, hogy jó-e. Erről ordít, hogy nem is létezik lassabb megoldás, úgyhogy le sem kódoltam, kapásból jobbat akartam készíteni.

### Egy a triviálisnál jelentősen gyorsabb megoldás

Az előző megoldásal az volt a baj, hogy ha az első sátort nem lehet északi irányba lerakni, akkor ennek ellenére még ki kell próbálni az összes megoldást ezzel a kombinációval, pedig azok már triviálisan nem lehetnek jók. Innen jött az ötlet, hogy egyesével rakjam le a sátrakat és minden lépés után ellenőrizzek, hogy lehet-e még egyáltalán jó, mert ha nem, akkor így meg tudom úszni az előbb említett felesleges próbálgatásokat.

## Megvalósítás

A megoldást az alábbi `satrak/1` függvény hajtja végre. Ez igazából csak egy csomagóló a tényleges `solve/6` függvényhez.

```ex
@spec satrak(pd :: puzzle_desc) :: [tent_dirs]
def satrak({tents_count_rows, tents_count_cols, trees}) do
  solve(tents_count_rows, tents_count_cols, Map.new(), MapSet.new(trees), trees, [])
end
```

Mielőtt viszont tovább haladnánk, szeretnék bemutatni két általam definiált típust.\
Az első egy halmaz, amit majd arra fogok használni, hogy eldöntsem, hogy egy adott mezőn áll-e fa.

```ex
@type tree_set :: MapSet.t()
```

A második egy map, amiben a kulcsok a sátrak pozícióját tartják számon, a hozzájuk tartozó érték párok pedig azt jelenti, hogy az adott sátor milyen irányban helyezkedik el a fájához képest.

```ex
@type tent_map :: %{field => dir}
```

Most már mindent tudunk ami szükséges a `solve/6` függvény megértéséhez.

```ex
@spec solve(
  tents_count_rows :: tents_count_rows,
  tents_count_cols :: tents_count_cols,
  tent_map :: tent_map,
  tree_set :: tree_set,
  trees :: trees,
  [tent_dirs]
) :: [tent_dirs]
```
- Az első két paraméter egyértelmű. Ezek a elvárt sátrak számát tartalmazó listák soronként illetve oszloponként.
- A harmadik paraméter a korábban ismertetett `tent_map` típus. Ez tartalmazza a már lehelyezett fákat.
- A negyedik paraméter a szintén korábban bemutatott `tree_set` Ez a megoldás során értelemszerűen sosem változik.
- Az ötödik paraméter a még sátor nélküli fák listája.
- A hatodik paraméter az eddig már megtalált helyes megoldásokat tartalmazza. Valójában csak egy akkumulátor ami a teljesítmény szempontjából szükséges, hogy ne listákat kelljen összefűznöm, hanem mindig egy elemet fűzhessek egy lista elejére.
- A visszetérési érték értelemszerűen a megoldások listája.

Két változatot írtam. Az elsőre akkor történik illesztés amikor még vannak sátor nélüli fák. Ebben az esetben veszem az első ilyen fát és végigmegyek az összes lehetséges irányon, mindegyikhez kiszámolva a sátor lehetséges helyét a `dir_and_tree_to_pos/2` függvénnyel. Ellenőrzöm, hogy található-e sátor vagy fa az adott mezőn. Ha nincsen ott semmi sem, akkor elhelyezem a sátrat ott és megnézem, a `pre_check/4` függvény segítségével, hogy lehet-e még jó a megoldás. Amennyiben a válasz igen, akkor rekurzívan hívom magamat újra, de most már paraméterként eggyel kevesebb fát tartalmazó listával és eggyel több sátrat tartalmazó mappel. Viszont ha bármilyen okból meghiusulás történt, azaz már áll a mezőn valami, vagy nem sikerült az előellenőrzés, akkor csak simán megyek tovább a következő irányra bármiféle rekurzív hívás nélkül.

```ex
defp solve(tents_count_rows, tents_count_cols, tent_map, tree_set, [tree | trees], acc) do
  [:n, :e, :s, :w]
  |> List.foldl(acc, fn dir, acc2 ->
    pos = dir_and_tree_to_pos(dir, tree)
    if tent_map |> Map.has_key?(pos) or tree_set |> MapSet.member?(pos) do
      acc2
    else
      new_tent_map = tent_map |> Map.put(pos, dir)
      if pre_check(tents_count_rows, tents_count_cols, new_tent_map, pos) do
        solve(tents_count_rows, tents_count_cols, new_tent_map, tree_set, trees, acc2)
      else
        acc2
      end
    end
  end)
end
```

A második változra akkor történik illesztés amikor már minden fához került sátor. Először van egy ellenőrzés, hogy a megoldás tényleg jó-e. Erre azért van szükség, mert eddig mindig arra történt az ellenőrzés, hogy lehet-e még jó a megoldás, aminek része volt a sorok és oszlopok sátorszámának ellenőrzése ahol csak azt néztük, hogy még nem léptük át a kívánt számot. Itt a végén viszont arra történik ellenőrzés, hogy elértük-e a kívánt számot. Amennyiben ez megvan, akkor a mapból előállítom a megoldást a kívánt formátumban és hozzáadom az akkumulátorhoz.

```ex
defp solve(tents_count_rows, tents_count_cols, tent_map, _, [], acc) do
  if check_tents_count_rows(tents_count_rows, tent_map, 1) and
        check_tents_count_cols(tents_count_cols, tent_map, 1) do
    solution =
      tent_map
      |> Map.to_list()
      |> List.foldl([], fn {{row, col}, char}, ds ->
        case char do
          :n -> [{{row + 1, col}, :n} | ds]
          :e -> [{{row, col - 1}, :e} | ds]
          :s -> [{{row - 1, col}, :s} | ds]
          :w -> [{{row, col + 1}, :w} | ds]
          _ -> ds
        end
      end)
      |> Enum.sort(fn
        {{row, col1}, _}, {{row, col2}, _} -> col1 <= col2
        {{row1, _}, _}, {{row2, _}, _} -> row1 <= row2
      end)
      |> Enum.map(fn {_, d} -> d end)
    [solution | acc]
  else
    acc
  end
end
```

A korábban említett `dir_and_tree_to_pos/2` függvénytípus specifikációja a következő:

```ex
@spec dir_and_tree_to_pos(dir :: dir, pos :: field) :: field
```

- Az első paraméter jelzi az irányt.
- A második meg, hogy hol van az a fa, amitől egyet el kell lépni az adott irányba.
  
Az implementálása szinte triviális.

```ex
defp dir_and_tree_to_pos(dir, {row, col}) do
  case dir do
    :n -> {row - 1, col}
    :e -> {row, col + 1}
    :s -> {row + 1, col}
    :w -> {row, col - 1}
  end
end
```

A `pre_check/4` függvényre viszont már érdemes ránézni.

```ex
@spec pre_check(
  tents_count_rows :: tents_count_rows,
  tents_count_cols :: tents_count_cols,
  tent_map :: tent_map,
  pos :: field
) :: boolean
```

- Az első három paraméter elég egyértelmű a korábbiak alapján.
- A negyedik viszont érdekes. A függvény lehetne helyes működésű nélküle is. Viszont mivel tudom, hogy egy lerakás hatására az adott sátor környezetében lévő mezőket kell csak ellenőrizni a mezők közül. A sorok és oszlopok küzül meg csak azt ahova raktam, ezért ennek megadásával jelentős számítás spórolható meg.

Két implementáció tartozik hozzá. Az első azt vizsgálja, hogy pályán kívülre történt-e a rakás amennyiben igen, már biztosan nem lehet jó a megoldásunk.

```ex
defp pre_check(tents_count_rows, tents_count_cols, _, {row, col})
      when row < 1 or col < 1 or row > length(tents_count_rows) or col > length(tents_count_cols),
      do: false
```

A második ellenőrzi a szomszédos mezőket, valamint a sorokat és oszlopokat.

```ex
defp pre_check(tents_count_rows, tents_count_cols, tent_map, {row, col}) do
  pre_check_adjacent_fields(tent_map, {row, col}) and
    pre_check_tent_count_row(tents_count_rows |> Enum.at(row - 1), tent_map, row) and
    pre_check_tent_count_col(tents_count_cols |> Enum.at(col - 1), tent_map, col)
end
```

Nézzük meg ezek implementációját is, kezdjük a `pre_check_adjacent_fields/2` függvénnyel!

```ex
@spec pre_check_adjacent_fields(tent_map :: tent_map, pos :: field) :: boolean
```

- Első paraméter a már lerakott sátrak mapja.
- Második a mező aminek a szomszédait ellenőrizni kell.

Az implementáció egyszerű favágás.

```ex
defp pre_check_adjacent_fields(tent_map, {row, col}) do
  has_adjacent =
    tent_map |> Map.has_key?({row - 1, col - 1}) or
      tent_map |> Map.has_key?({row - 1, col}) or
      tent_map |> Map.has_key?({row - 1, col + 1}) or
      tent_map |> Map.has_key?({row, col + 1}) or
      tent_map |> Map.has_key?({row + 1, col + 1}) or
      tent_map |> Map.has_key?({row + 1, col}) or
      tent_map |> Map.has_key?({row + 1, col - 1}) or
      tent_map |> Map.has_key?({row, col - 1})
  not has_adjacent
end
```

A `pre_check_tent_count_row/3` és `pre_check_tent_count_col/3` függvények közül elég csak megnézni az egyiket.

```ex
@spec pre_check_tent_count_row(
  expected_tent_count_row :: integer,
  tent_map :: tent_map,
  row :: integer
) :: boolean
```

- Első paraméter az elvárt szám a sorban.
- Második a már jól ismert map.
- Harmadik meg, hogy melyik sorban kell majd ellenőrizni.

Ennek is két verziója van. Az első arra az esetre amikor nem számít, hogy mennyi sátor lesz a sorban.

```ex
defp pre_check_tent_count_row(expected_tent_count_row, _, _) when expected_tent_count_row < 0,
  do: true
```

A második meg a tényleges ellenőrzést végzi.

```ex
defp pre_check_tent_count_row(expected_tent_count_row, map, row),
  do: tent_count_row(map, row) <= expected_tent_count_row
```

Ez használta a `tent_count_row/2` függvényt, nézzünk rá erre is!

```ex
@spec tent_count_row(tent_map :: tent_map, row :: integer) :: non_neg_integer
```

- Az első paraméter a kedvenc mapünk.
- A második meg, hogy melyik sorban kell összeszámolni a fákat.

Az implementálás magától értetődő.

```ex
defp tent_count_row(tent_map, row) do
  tent_map
  |> Map.keys()
  |> Enum.filter(fn {key_row, _} -> key_row === row end)
  |> length()
end
```

Két függvény van amiről még nem esett szó. A `check_tents_count_rows/3` és a `check_tents_count_cols/3`
Innen is nézzük meg az egyiket!

```ex
@spec check_tents_count_rows(
  tents_count_rows :: tents_count_rows,
  tent_map :: tent_map,
  row :: integer
) :: boolean
```

- Első paraméter a soronént elvárt sátrak számának listája.
- Második a map.
- Harmadik meg, hogy meilyk sortól indulva ellenőrizzen. Nem olyan szép, hogy itt van, erre egy segéd függvényt illett volna írni, de úgy is csak egy helyen használom, jó az így.

Három különbőző implementáció is tartozik ide. Az első a triviális eset. Ha nincsen elvárt sátorszám, akkor tuti, hogy jók vagyunk.

```ex
defp check_tents_count_rows([], _, _), do: true
```

A második eset is nagyon egyszerű. Ha nicsen specifikálva az elvárt sátrak száma, akkor nézhetjük a következő sort.

```ex
defp check_tents_count_rows([tents_count_row | tents_count_rows], tent_map, row)
      when tents_count_row < 0,
      do: check_tents_count_rows(tents_count_rows, tent_map, row + 1)
```

Ha viszont meg van adva, akkor meg kell számolni, hogy mennyi sátor van az adott sorban és utána menni a következő sorra. Fontos a sorrend! Elixirben lusta kiértékelés van, csak akkor megyek tovább a következő sorra, ha az adott sor jó. Ezért használok itt rekurziót reduce/fold helyett.

```ex
defp check_tents_count_rows([tents_count_row | tents_count_rows], tent_map, row) do
  tent_count_row(tent_map, row) === tents_count_row and
    check_tents_count_rows(tents_count_rows, tent_map, row + 1)
end
```
