:- use_module(library(lists)).

intersection(Set1, Set2, Set3) :-
    findall(
        Element,
        (
            member(Element, Set1),
            member(Element, Set2)
        ),
        Set3
    ).

biztos_sor(Sor1-_, Sor, [s]) :-
    Sor1 is Sor - 1.

biztos_sor(Sor-_, Sor, IL) :-
    \+ member(s, IL),
    \+ member(n, IL).

biztos_sor(Sor1-_, Sor, [n]) :-
    Sor1 is Sor + 1.

bistos_sor_szamol([], _, [], 0).

bistos_sor_szamol([F | Fs], J, [IL | ILs], B) :-
    biztos_sor(F, J, IL),
    bistos_sor_szamol(Fs, J, ILs, B1),
    B is B1 + 1,
    !.

bistos_sor_szamol([_ | Fs], J, [_ | ILs], B) :-
    bistos_sor_szamol(Fs, J, ILs, B).

esetleges_sor_seged(Sor1-_, Sor, IL) :-
    Sor1 is Sor - 1,
    member(s, IL).

esetleges_sor_seged(Sor-_, Sor, IL) :-
    (   member(e, IL)
    ;   member(w, IL)
    ).

esetleges_sor_seged(Sor1-_, Sor, IL) :-
    Sor1 is Sor + 1,
    member(n, IL).

esetleges_sor(F, J, IL) :-
    \+ biztos_sor(F, J, IL),
    esetleges_sor_seged(F, J, IL).

esetleges_sor_szamol([], _, [], 0).

esetleges_sor_szamol([F | Fs], J, [IL | ILs], B) :-
    esetleges_sor(F, J, IL),
    esetleges_sor_szamol(Fs, J, ILs, B1),
    B is B1 + 1,
    !.

esetleges_sor_szamol([_ | Fs], J, [_ | ILs], B) :-
    esetleges_sor_szamol(Fs, J, ILs, B),
    !.

sor_ii_seged(Sor1-_, Sor, IL0, [s]) :-
    member(s, IL0),
    Sor1 is Sor - 1,
    !.

sor_ii_seged(Sor-_, Sor, IL0, IL) :-
    (   member(w, IL0)
    ;   member(e, IL0)
    ),
    intersection(IL0, [w, e], IL),
    !.

sor_ii_seged(Sor1-_, Sor, IL0, [n]) :-
    member(n, IL0),
    Sor1 is Sor + 1,
    !.

sor_ii_seged(_, _, IL, IL).

sor_ii([], _, [], []).

sor_ii([F | Fs], J, [IL0 | ILs0], [IL | ILs]) :-
    sor_ii_seged(F, J, IL0, IL),
    sor_ii(Fs, J, ILs0, ILs).

sor_iii_seged(Sor1-_, Sor, IL0, IL) :-
    Sor1 is Sor - 1,
    \+ IL0 = [s],
    member(s, IL0),
    intersection(IL0, [n, e, w], IL),
    !.

sor_iii_seged(Sor-_, Sor, IL0, IL) :-
    \+ intersection(IL0, [e, w], []),
    intersection(IL0, [s, n], IL),
    \+ IL = [],
    !.

sor_iii_seged(Sor1-_, Sor, IL0, IL) :-
    Sor1 is Sor + 1,
    \+ IL0 = [n],
    member(n, IL0),
    intersection(IL0, [s, e, w], IL),
    !.

sor_iii_seged(_, _, IL, IL).

sor_iii([], _, [], []).

sor_iii([F | Fs], J, [IL0 | ILs0], [IL | ILs]) :-
    sor_iii_seged(F, J, IL0, IL),
    sor_iii(Fs, J, ILs0, ILs).

sor_szukites(Fs, J, Db, ILs0, ILs) :- 
    bistos_sor_szamol(Fs, J, ILs0, B),
    esetleges_sor_szamol(Fs, J, ILs0, E),
    S is B + E,
    (   S < Db ->  ILs = []
    ;   S =:= Db ->  sor_ii(Fs, J, ILs0, ILs)
    ;   B =:= Db ->  sor_iii(Fs, J, ILs0, ILs)
    ;   B > Db ->  ILs = []
    ).

biztos_oszlop(_-Oszlop1, Oszlop, [e]) :-
    Oszlop1 is Oszlop - 1.

biztos_oszlop(_-Oszlop, Oszlop, IL) :-
    \+ member(e, IL),
    \+ member(w, IL).

biztos_oszlop(_-Oszlop1, Oszlop, [w]) :-
    Oszlop1 is Oszlop + 1.

bistos_oszlop_szamol([], _, [], 0).

bistos_oszlop_szamol([F | Fs], J, [IL | ILs], B) :-
    biztos_oszlop(F, J, IL),
    bistos_oszlop_szamol(Fs, J, ILs, B1),
    B is B1 + 1,
    !.

bistos_oszlop_szamol([_ | Fs], J, [_ | ILs], B) :-
    bistos_oszlop_szamol(Fs, J, ILs, B).

esetleges_oszlop_seged(_-Oszlop1, Oszlop, IL) :-
    Oszlop1 is Oszlop - 1,
    member(e, IL).

esetleges_oszlop_seged(_-Oszlop, Oszlop, IL) :-
    (   member(s, IL)
    ;   member(n, IL)
    ).

esetleges_oszlop_seged(_-Oszlop1, Oszlop, IL) :-
    Oszlop1 is Oszlop + 1,
    member(w, IL).

esetleges_oszlop(F, J, IL) :-
    \+ biztos_oszlop(F, J, IL),
    esetleges_oszlop_seged(F, J, IL).

esetleges_oszlop_szamol([], _, [], 0).

esetleges_oszlop_szamol([F | Fs], J, [IL | ILs], B) :-
    esetleges_oszlop(F, J, IL),
    esetleges_oszlop_szamol(Fs, J, ILs, B1),
    B is B1 + 1,
    !.

esetleges_oszlop_szamol([_ | Fs], J, [_ | ILs], B) :-
    esetleges_oszlop_szamol(Fs, J, ILs, B),
    !.

oszlop_ii_seged(_-Oszlop1, Oszlop, IL0, [e]) :-
    member(e, IL0),
    Oszlop1 is Oszlop - 1,
    !.

oszlop_ii_seged(_-Oszlop, Oszlop, IL0, IL) :-
    (   member(n, IL0)
    ;   member(s, IL0)
    ),
    intersection(IL0, [n, s], IL),
    !.

oszlop_ii_seged(_-Oszlop1, Oszlop, IL0, [w]) :-
    member(w, IL0),
    Oszlop1 is Oszlop + 1,
    !.

oszlop_ii_seged(_, _, IL, IL).

oszlop_ii([], _, [], []).

oszlop_ii([F | Fs], J, [IL0 | ILs0], [IL | ILs]) :-
    oszlop_ii_seged(F, J, IL0, IL),
    oszlop_ii(Fs, J, ILs0, ILs).

oszlop_iii_seged(_-Oszlop1, Oszlop, IL0, IL) :-
    Oszlop1 is Oszlop - 1,
    \+ IL0 = [e],
    member(e, IL0),
    intersection(IL0, [w, s, n], IL),
    !.

oszlop_iii_seged(_-Oszlop, Oszlop, IL0, IL) :-
    \+ intersection(IL0, [s, n], []),
    intersection(IL0, [e, w], IL),
    \+ IL = [],
    !.

oszlop_iii_seged(_-Oszlop1, Oszlop, IL0, IL) :-
    Oszlop1 is Oszlop + 1,
    \+ IL0 = [w],
    member(w, IL0),
    intersection(IL0, [e, s, n], IL),
    !.

oszlop_iii_seged(_, _, IL, IL).

oszlop_iii([], _, [], []).

oszlop_iii([F | Fs], J, [IL0 | ILs0], [IL | ILs]) :-
    oszlop_iii_seged(F, J, IL0, IL),
    oszlop_iii(Fs, J, ILs0, ILs).

oszlop_szukites(Fs, J, Db, ILs0, ILs) :- 
    bistos_oszlop_szamol(Fs, J, ILs0, B),
    esetleges_oszlop_szamol(Fs, J, ILs0, E),
    S is B + E,
    (   S < Db ->  ILs = []
    ;   S =:= Db ->  oszlop_ii(Fs, J, ILs0, ILs)
    ;   B =:= Db ->  oszlop_iii(Fs, J, ILs0, ILs)
    ;   B > Db ->  ILs = []
    ).

osszeg_szukites(Fs, sor(I,Db), ILs0, ILs) :-
    sor_szukites(Fs, I, Db, ILs0, ILs).

osszeg_szukites(Fs, oszl(J,Db), ILs0, ILs) :-
    oszlop_szukites(Fs, J, Db, ILs0, ILs).
