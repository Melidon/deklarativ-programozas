:- use_module(library(lists)).

% khf5.pl

fabol_es_iranybol_sator(ROW-COL, e, ROW-COL1) :- COL1 is COL+1.
fabol_es_iranybol_sator(ROW-COL, n, ROW1-COL) :- ROW1 is ROW-1.
fabol_es_iranybol_sator(ROW-COL, s, ROW1-COL) :- ROW1 is ROW+1.
fabol_es_iranybol_sator(ROW-COL, w, ROW-COL1) :- COL1 is COL-1.

szabad_hely(N-M, [], ROW-COL) :-
    0 < ROW, ROW =< N,
    0 < COL, COL =< M.
szabad_hely(NM, [F|Fs], S) :-
    \+ F = S,
    szabad_hely(NM, Fs, S).

iranylistak_seged(_, _, [], []).
iranylistak_seged(NM, Fs, [F | Maradek], [IL | ILs]) :-
    findall(I, (fabol_es_iranybol_sator(F, I, S), szabad_hely(NM, Fs, S)), IL),
    iranylistak_seged(NM, Fs, Maradek, ILs).

iranylistak(NM, Fs, RILs) :-
    iranylistak_seged(NM, Fs, Fs, ILs),
    (   member([], ILs) ->  RILs = []
    ;   RILs = ILs
    ).

oldalszomszed(MINEK, MI) :- fabol_es_iranybol_sator(MINEK, _, MI).

sarokszomszed(ROW-COL, ROW1-COL1) :- ROW1 is ROW-1, COL1 is COL+1.
sarokszomszed(ROW-COL, ROW1-COL1) :- ROW1 is ROW+1, COL1 is COL+1.
sarokszomszed(ROW-COL, ROW1-COL1) :- ROW1 is ROW+1, COL1 is COL-1.
sarokszomszed(ROW-COL, ROW1-COL1) :- ROW1 is ROW-1, COL1 is COL-1.

szomszed(MINEK, MI) :-
    oldalszomszed(MINEK, MI) ; sarokszomszed(MINEK, MI).

szukit(_, [], [], []).
szukit(Nemszabadok, [Fa|Fak], [IL0|ILs0], [IL|ILs]) :-
    findall(
        Irany,
        (
            member(Irany, IL0),
            fabol_es_iranybol_sator(Fa, Irany, Sator),
            \+ member(Sator, Nemszabadok)
        ),
        IL
    ),
    szukit(Nemszabadok, Fak, ILs0, ILs).
    
sator_szukites(Fak, Index, ILs0, ILs) :-
    nth1(Index, ILs0, [Irany], MaradekIranyok),
    nth1(Index, Fak, Fa, MaradekFak),
    fabol_es_iranybol_sator(Fa, Irany, Sator),
    findall(Szomszed, szomszed(Sator, Szomszed), Szomszedok),
    Nemszabadok = [Sator | Szomszedok],
    szukit(Nemszabadok, MaradekFak, MaradekIranyok, SzukitettIranyok),
    nth1(Index, Eredmeny, [Irany], SzukitettIranyok),
    (   member([], Eredmeny) ->  ILs = []
    ;   ILs = Eredmeny
    ).

% khf6.pl

intersection(Set1, Set2, Set3) :-
    findall(
        Element,
        (
            member(Element, Set1),
            member(Element, Set2)
        ),
        Set3
    ).

biztos_sor(Sor1-_, Sor, [s]) :-
    Sor1 is Sor - 1.

biztos_sor(Sor-_, Sor, IL) :-
    \+ member(s, IL),
    \+ member(n, IL).

biztos_sor(Sor1-_, Sor, [n]) :-
    Sor1 is Sor + 1.

bistos_sor_szamol([], _, [], 0).

bistos_sor_szamol([F | Fs], J, [IL | ILs], B) :-
    biztos_sor(F, J, IL),
    bistos_sor_szamol(Fs, J, ILs, B1),
    B is B1 + 1,
    !.

bistos_sor_szamol([_ | Fs], J, [_ | ILs], B) :-
    bistos_sor_szamol(Fs, J, ILs, B).

esetleges_sor_seged(Sor1-_, Sor, IL) :-
    Sor1 is Sor - 1,
    member(s, IL).

esetleges_sor_seged(Sor-_, Sor, IL) :-
    (   member(e, IL)
    ;   member(w, IL)
    ).

esetleges_sor_seged(Sor1-_, Sor, IL) :-
    Sor1 is Sor + 1,
    member(n, IL).

esetleges_sor(F, J, IL) :-
    \+ biztos_sor(F, J, IL),
    esetleges_sor_seged(F, J, IL).

esetleges_sor_szamol([], _, [], 0).

esetleges_sor_szamol([F | Fs], J, [IL | ILs], B) :-
    esetleges_sor(F, J, IL),
    esetleges_sor_szamol(Fs, J, ILs, B1),
    B is B1 + 1,
    !.

esetleges_sor_szamol([_ | Fs], J, [_ | ILs], B) :-
    esetleges_sor_szamol(Fs, J, ILs, B),
    !.

sor_ii_seged(Sor1-_, Sor, IL0, [s]) :-
    member(s, IL0),
    Sor1 is Sor - 1,
    !.

sor_ii_seged(Sor-_, Sor, IL0, IL) :-
    (   member(w, IL0)
    ;   member(e, IL0)
    ),
    intersection(IL0, [w, e], IL),
    !.

sor_ii_seged(Sor1-_, Sor, IL0, [n]) :-
    member(n, IL0),
    Sor1 is Sor + 1,
    !.

sor_ii_seged(_, _, IL, IL).

sor_ii([], _, [], []).

sor_ii([F | Fs], J, [IL0 | ILs0], [IL | ILs]) :-
    sor_ii_seged(F, J, IL0, IL),
    sor_ii(Fs, J, ILs0, ILs).

sor_iii_seged(Sor1-_, Sor, IL0, IL) :-
    Sor1 is Sor - 1,
    \+ IL0 = [s],
    member(s, IL0),
    intersection(IL0, [n, e, w], IL),
    !.

sor_iii_seged(Sor-_, Sor, IL0, IL) :-
    \+ intersection(IL0, [e, w], []),
    intersection(IL0, [s, n], IL),
    \+ IL = [],
    !.

sor_iii_seged(Sor1-_, Sor, IL0, IL) :-
    Sor1 is Sor + 1,
    \+ IL0 = [n],
    member(n, IL0),
    intersection(IL0, [s, e, w], IL),
    !.

sor_iii_seged(_, _, IL, IL).

sor_iii([], _, [], []).

sor_iii([F | Fs], J, [IL0 | ILs0], [IL | ILs]) :-
    sor_iii_seged(F, J, IL0, IL),
    sor_iii(Fs, J, ILs0, ILs).

sor_szukites(Fs, J, Db, ILs0, ILs) :- 
    bistos_sor_szamol(Fs, J, ILs0, B),
    esetleges_sor_szamol(Fs, J, ILs0, E),
    S is B + E,
    (   S < Db ->  ILs = []
    ;   S =:= Db ->  sor_ii(Fs, J, ILs0, ILs)
    ;   B =:= Db ->  sor_iii(Fs, J, ILs0, ILs)
    ;   B > Db ->  ILs = []
    ).

biztos_oszlop(_-Oszlop1, Oszlop, [e]) :-
    Oszlop1 is Oszlop - 1.

biztos_oszlop(_-Oszlop, Oszlop, IL) :-
    \+ member(e, IL),
    \+ member(w, IL).

biztos_oszlop(_-Oszlop1, Oszlop, [w]) :-
    Oszlop1 is Oszlop + 1.

bistos_oszlop_szamol([], _, [], 0).

bistos_oszlop_szamol([F | Fs], J, [IL | ILs], B) :-
    biztos_oszlop(F, J, IL),
    bistos_oszlop_szamol(Fs, J, ILs, B1),
    B is B1 + 1,
    !.

bistos_oszlop_szamol([_ | Fs], J, [_ | ILs], B) :-
    bistos_oszlop_szamol(Fs, J, ILs, B).

esetleges_oszlop_seged(_-Oszlop1, Oszlop, IL) :-
    Oszlop1 is Oszlop - 1,
    member(e, IL).

esetleges_oszlop_seged(_-Oszlop, Oszlop, IL) :-
    (   member(s, IL)
    ;   member(n, IL)
    ).

esetleges_oszlop_seged(_-Oszlop1, Oszlop, IL) :-
    Oszlop1 is Oszlop + 1,
    member(w, IL).

esetleges_oszlop(F, J, IL) :-
    \+ biztos_oszlop(F, J, IL),
    esetleges_oszlop_seged(F, J, IL).

esetleges_oszlop_szamol([], _, [], 0).

esetleges_oszlop_szamol([F | Fs], J, [IL | ILs], B) :-
    esetleges_oszlop(F, J, IL),
    esetleges_oszlop_szamol(Fs, J, ILs, B1),
    B is B1 + 1,
    !.

esetleges_oszlop_szamol([_ | Fs], J, [_ | ILs], B) :-
    esetleges_oszlop_szamol(Fs, J, ILs, B),
    !.

oszlop_ii_seged(_-Oszlop1, Oszlop, IL0, [e]) :-
    member(e, IL0),
    Oszlop1 is Oszlop - 1,
    !.

oszlop_ii_seged(_-Oszlop, Oszlop, IL0, IL) :-
    (   member(n, IL0)
    ;   member(s, IL0)
    ),
    intersection(IL0, [n, s], IL),
    !.

oszlop_ii_seged(_-Oszlop1, Oszlop, IL0, [w]) :-
    member(w, IL0),
    Oszlop1 is Oszlop + 1,
    !.

oszlop_ii_seged(_, _, IL, IL).

oszlop_ii([], _, [], []).

oszlop_ii([F | Fs], J, [IL0 | ILs0], [IL | ILs]) :-
    oszlop_ii_seged(F, J, IL0, IL),
    oszlop_ii(Fs, J, ILs0, ILs).

oszlop_iii_seged(_-Oszlop1, Oszlop, IL0, IL) :-
    Oszlop1 is Oszlop - 1,
    \+ IL0 = [e],
    member(e, IL0),
    intersection(IL0, [w, s, n], IL),
    !.

oszlop_iii_seged(_-Oszlop, Oszlop, IL0, IL) :-
    \+ intersection(IL0, [s, n], []),
    intersection(IL0, [e, w], IL),
    \+ IL = [],
    !.

oszlop_iii_seged(_-Oszlop1, Oszlop, IL0, IL) :-
    Oszlop1 is Oszlop + 1,
    \+ IL0 = [w],
    member(w, IL0),
    intersection(IL0, [e, s, n], IL),
    !.

oszlop_iii_seged(_, _, IL, IL).

oszlop_iii([], _, [], []).

oszlop_iii([F | Fs], J, [IL0 | ILs0], [IL | ILs]) :-
    oszlop_iii_seged(F, J, IL0, IL),
    oszlop_iii(Fs, J, ILs0, ILs).

oszlop_szukites(Fs, J, Db, ILs0, ILs) :- 
    bistos_oszlop_szamol(Fs, J, ILs0, B),
    esetleges_oszlop_szamol(Fs, J, ILs0, E),
    S is B + E,
    (   S < Db ->  ILs = []
    ;   S =:= Db ->  oszlop_ii(Fs, J, ILs0, ILs)
    ;   B =:= Db ->  oszlop_iii(Fs, J, ILs0, ILs)
    ;   B > Db ->  ILs = []
    ).

osszeg_szukites(Fs, sor(I,Db), ILs0, ILs) :-
    sor_szukites(Fs, I, Db, ILs0, ILs).

osszeg_szukites(Fs, oszl(J,Db), ILs0, ILs) :-
    oszlop_szukites(Fs, J, Db, ILs0, ILs).

% nhf2

minden_sator_szabaly(_, [], []).
minden_sator_szabaly(Idx, [_|Fak], [sator(Idx)|Szabalyok]) :-
    Idx1 is Idx + 1,
    minden_sator_szabaly(Idx1,Fak,Szabalyok).
    
minden_sor_szabaly(_, [], []).
minden_sor_szabaly(I, [S|Ss], Kimenet) :-
    I1 is I + 1,
    minden_sor_szabaly(I1, Ss, Szabalyok),
    (   S < 0 ->  Kimenet = Szabalyok
    ;   Kimenet = [sor(I,S)|Szabalyok]
    ).

minden_oszlop_szabaly(_, [], []).
minden_oszlop_szabaly(J, [O|Os], Kimenet) :-
    J1 is J + 1,
    minden_oszlop_szabaly(J1, Os, Szabalyok),
    (   O < 0 -> Kimenet = Szabalyok
    ;   Kimenet = [oszl(J,O)|Szabalyok]
    ).

minden_szabaly(Ss, Os, Fak, Szabalyok) :-
    minden_sor_szabaly(1, Ss, Sorszabalyok),
    minden_oszlop_szabaly(1, Os, Oszlopszabalyok),
    append(Sorszabalyok, Oszlopszabalyok, Osszegszabalyok),
    minden_sator_szabaly(1, Fak, Satorszabalyok),
    append(Osszegszabalyok, Satorszabalyok, Szabalyok).

alkalmaz(Fs, Szabaly, ILs0, ILs) :-
    (   sator(Idx) = Szabaly -> sator_szukites(Fs, Idx, ILs0, ILs)
    ;	osszeg_szukites(Fs, Szabaly, ILs0, ILs)
    ).

mindet_szukit(Fak, [Szabaly|Szabalyok0], ILs0, Szabalyok, ILs) :-
    (   ILs0 = [] ->  ILs = [], Szabalyok = []
    ;	alkalmaz(Fak, Szabaly, ILs0, ILs1) ->
    	(	mindet_szukit(Fak, Szabalyok0, ILs1, Szabalyok1, ILs2) ->
        	ILs = ILs2, Szabalyok = Szabalyok1
        ;   ILs = ILs1, Szabalyok = Szabalyok0
        )
    ;	Szabalyok = [Szabaly|Szabalyok1], mindet_szukit(Fak, Szabalyok0, ILs0, Szabalyok1, ILs)
    ).

szukit_amig_tud(Fak, Szabalyok0, ILs0, Szabalyok, ILs) :-
    \+ ILs0 = [],
    (	mindet_szukit(Fak, Szabalyok0, ILs0, Szabalyok1, ILs1) -> szukit_amig_tud(Fak, Szabalyok1, ILs1, Szabalyok, ILs)
    ;   ILs = ILs0, Szabalyok = Szabalyok0
    ).

valaszt([IL|ILs0], ILs) :-
    (   IL = [I]	->  valaszt(ILs0, ILs1), ILs = [[I]|ILs1]
    ;   member(I, IL), ILs = [[I]|ILs0]
    ).

flatten_ILs([], []).
flatten_ILs([[I]|ILs], [I|Is]) :- flatten_ILs(ILs, Is).

megold(Fak, Szabalyok0, ILs0, Is) :-
    szukit_amig_tud(Fak, Szabalyok0, ILs0, Szabalyok, ILs1),
    (   flatten_ILs(ILs1, Is1) ->  Is = Is1
    ;   valaszt(ILs1, ILs2), megold(Fak, Szabalyok, ILs2, Is)
    ).

satrak(satrak(Ss, Os, Fak), Is) :-
    length(Ss, N),
	length(Os, M),
    iranylistak(N-M, Fak, ILs),
    minden_szabaly(Ss, Os, Fak, Szabalyok),
	megold(Fak, Szabalyok, ILs, Is).
