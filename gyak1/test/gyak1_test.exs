defmodule Gyak1Test do
  use ExUnit.Case

  test "lnko test" do
    assert Gyak1.Lnko.lnko(96, 42) === 6
  end

  test "len test" do
    assert Gyak1.Len.len([]) === 0
    assert Gyak1.Len.len('hosszú_karakterlista') === 20
  end

  test "leni test" do
    assert Gyak1.Len.leni([]) === 0
    assert Gyak1.Len.leni('hosszú_karakterlista') === 20
  end

  test "pi test" do
    # assert abs(Gyak1.Pi.pi(10000000) - :math.pi) < 1.0e-6
  end

  test "dec2rad test" do
    assert Gyak1.Dec2rad.dec2rad(2, 13) === [1, 1, 0, 1]
    assert Gyak1.Dec2rad.dec2rad(17, 127) === [7, 8]
  end

  test "last_er test" do
    assert Gyak1.Last.last_er([5, 1, 2, 8, 7]) === {:ok, 7}
    assert Gyak1.Last.last_er([]) === :error
  end

  test "last_ex test" do
    assert Gyak1.Last.last_ex([5, 1, 2, 8, 7]) === 7
    assert Gyak1.Last.last_ex([]) === nil
  end

  test "at test" do
    assert Gyak1.At.at([:a, :b, :c], 2) === :c
  end

  test "split test" do
    assert Gyak1.Split.split([10, 20, 30, 40, 50], 3) === {[10, 20, 30], [40, 50]}
  end

  test "take test" do
    assert Gyak1.Take.take([10, 20, 30, 40, 50], 3) === [10, 20, 30]
  end

  test "drop test" do
    assert Gyak1.Drop.drop([10, 20, 30, 40, 50], 3) === [40, 50]
  end

  test "tails test" do
    assert Gyak1.Tails.tails([1, 4, 2]) === [[1, 4, 2], [4, 2], [2], []]

    assert Gyak1.Tails.tails([:a, :b, :c, :d]) === [
             [:a, :b, :c, :d],
             [:b, :c, :d],
             [:c, :d],
             [:d],
             []
           ]
  end

  test "parban test" do
    assert Gyak1.Parban.parban([:a, :a, :a, 2, 3, 3, :a, 2, :b, :b, 4, 4]) === [:a, :a, 3, :b, 4]
  end

  test "vertekek test" do
    assert Gyak1.Vertekek.vertekek([:alma, {:s, 3}, {:v, 1}, 3, {:v, 2}]) === [1, 2]
  end

  test "dadogo test" do
    assert Gyak1.Dadogo.dadogo([:a, :a, :a, 2, 3, 3, :a, :b, :b, :b, :b]) === [
             [:a],
             [:a],
             [3],
             [:b],
             [:b, :b],
             [:b],
             [:b]
           ]
  end
end
