% 1. Atomok szeletelése

prefix(_, [], 0).

prefix([L | Ls], [L | Ps], N) :-
    N1 is N - 1,
    prefix(Ls, Ps, N1).

atom_prefix(Atom, AtomPrefix, N) :-
    atom_codes(Atom, Lista),
    prefix(Lista, ListaPrefix, N),
    atom_codes(AtomPrefix, ListaPrefix).

% 2. Általános Prolog kifejezés bizonyos részkifejezéseinek felsorolása

reszatom(A, A) :-
    atom(A).

reszatom(K, A) :-
    nonvar(K),
    K =.. [_ | KL],
    member(K1, KL),
    reszatom(K1, A).

% 3. Általános Prolog kifejezés bizonyos részkifejezéseinek akkumulálása

osszege(Szam, Szam) :-
    number(Szam), !.

osszege(K, Osszeg) :-
    compound(K),
    K =.. [_ | KL],
    findall(
        Reszosszeg,
        (
        	member(K1, KL),
            osszege(K1, Reszosszeg)
        ),
        Reszosszeglista
    ),
    foldl(plus, Reszosszeglista, 0, Osszeg), !.

osszege(_, 0).
