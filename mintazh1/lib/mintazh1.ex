defmodule Mintazh1 do
  def fel1 do
    x1 = {{:some, 6 + 1}, [?A] ++ 'B', length([&Kernel.elem/2]), (&Kernel.>/2).(2, 3)}
    IO.inspect(x1)
    x2 = Enum.map(Enum.concat(['xy7', 'a5', '8zx']), fn x -> x === ?x end)
    IO.inspect(x2)
    f = &../2

    x3 =
      for(x <- f.(1, 2), y <- f.(3, 1), x <= y, do: f.(x, y))
      |> Enum.map(&Enum.to_list/1)
      |> Enum.concat()
      |> Enum.uniq()

    IO.inspect(x3)
  end

  defp kernel_hd([x | _xs]), do: x
  defp kernel_tl([_x | xs]), do: xs
  defp list_foldl([], acc, _), do: acc

  defp list_foldl(list, acc, fun),
    do: list_foldl(kernel_tl(list), fun.(kernel_hd(list), acc), fun)

  defp kernel_max(first, second), do: if(first > second, do: first, else: second)

  defp enum_max(list),
    do: list_foldl(kernel_tl(list), kernel_hd(list), fn x, acc -> kernel_max(x, acc) end)

  @spec szigetek(xs :: [integer]) :: rss :: [[integer]]
  # Az xs pozitív elemekből álló, folytonos, maximális
  # hosszúságú részlistáinak listája az rss lista.

  def szigetek([x1 | xs]) when x1 <= 0, do: szigetek(xs)
  def szigetek([x1, x2 | xs]) when x2 <= 0, do: [[x1] | szigetek(xs)]

  def szigetek([x1, x2 | xs]) do
    [rs | rss] = szigetek([x2 | xs])
    [[x1 | rs] | rss]
  end

  def szigetek([x1]), do: [[x1]]
  def szigetek([]), do: []

  @spec szfold(zs :: [integer]) :: ys :: [{hossz :: integer, csucs :: integer}]
  # A negatív számokat nem tartalmazó zs egészlistában előforduló szárazföldek
  # hosszát és legmagasabb pontját leíró {hossz, csucs} párok listája ys.
  def szfold(zs), do: for(rs <- szigetek(zs), do: {length(rs), enum_max(rs)})

  def dec2hex_convert(d) when d < 10, do: ?0 + d
  def dec2hex_convert(d), do: ?A - 10 + d
  def dec2hex_helper(0, acc), do: acc
  def dec2hex_helper(d, acc), do: dec2hex_helper(div(d, 16), [dec2hex_convert(rem(d, 16)) | acc])

  @spec dec2hex(d :: integer) :: h :: charlist
  # A d decimális szám ?0-?9 és ?A-?F karakterek füzéreként ábrázolt
  # hexadecimális megfelelője h.
  def dec2hex(d) when d === 0, do: [dec2hex_convert(0)]
  def dec2hex(d), do: dec2hex_helper(d, [])

  @type fa(elem) :: :e | {:n, elem, fa(elem), fa(elem)}
  @spec d2hfa(fa0 :: fa(integer)) :: fa :: fa(charlist)
  # A fa0 decimális számokat tartalmazó fa hexadecimális számokat
  # tartalmazó megfelelője fa.
  def d2hfa(:e), do: :e
  def d2hfa({:n, elem, bal, jobb}), do: {:n, dec2hex(elem), d2hfa(bal), d2hfa(jobb)}
end
