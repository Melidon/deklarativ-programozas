defmodule Khf2Test do
  use ExUnit.Case
  doctest Khf2

  test "to_external test0" do
    puzzle0 = {[-1, 0, 0, -3, 0], [0, 0, -2, 0, 0], []}
    assert Khf2.to_external(puzzle0, [], "khf2_r0.txt") == :ok
  end

  test "to_external test1" do
    puzzle1 = {[1, 1, 0, 3, 0], [1, 0, 2, 0, 2], [{1, 2}, {3, 3}, {3, 5}, {5, 1}, {5, 5}]}
    assert Khf2.to_external(puzzle1, [:e, :s, :n, :n, :n], "khf2_r1.txt") == :ok
  end

  test "to_external test2" do
    puzzle2 = {[1, 1, -1, 3, 0], [1, 0, -2, 0, 2], [{1, 2}, {3, 3}, {3, 5}, {5, 1}, {5, 5}]}
    assert Khf2.to_external(puzzle2, [:e, :s, :n, :n, :w], "khf2_r2.txt") == :ok
  end

  test "to_external test3" do
    puzzle3 = {[2], [0, 1, -1, 0, -1], [{1, 1}, {1, 4}]}
    assert Khf2.to_external(puzzle3, [:e, :e], "khf2_r3.txt") == :ok
  end

  test "to_external test4" do
    puzzle4 = {[0, -1, 0, 1, 1, 0], [3], [{1, 1}, {3, 1}, {6, 1}]}
    assert Khf2.to_external(puzzle4, [:s, :s, :n], "khf2_r4.txt") == :ok
  end
end
