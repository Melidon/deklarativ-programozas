defmodule Khf3 do
  @moduledoc """
  Kemping térképe
  @author "Szommer Zsombor <zsombor.szommer@protonmail.com>"
  @date   "2022-10-08"
  ...
  """
  # sor száma (1-től n-ig)
  @type row :: integer
  # oszlop száma (1-től m-ig)
  @type col :: integer
  # egy parcella koordinátái
  @type field :: {row, col}

  # a sátrak száma soronként
  @type tents_count_rows :: [integer]
  # a sátrak száma oszloponként
  @type tents_count_cols :: [integer]
  # a fákat tartalmazó parcellák koordinátái lexikálisan rendezve
  @type trees :: [field]
  # a feladványleíró hármas
  @type puzzle_desc :: {tents_count_rows, tents_count_cols, trees}

  # a sátorpozíciók iránya: north, east, south, west
  @type dir :: :n | :e | :s | :w
  # a sátorpozíciók irányának listája a fákhoz képest
  @type tent_dirs :: [dir]

  # a fák száma a kempingben
  @type cnt_tree :: integer
  # az elemek száma a sátorpozíciók irányának listájában
  @type cnt_tent :: integer
  # a sátrak száma rossz a felsorolt sorokban
  @type err_rows :: %{err_rows: [integer]}
  # a sátrak száma rossz a felsorolt oszlopokban
  @type err_cols :: %{err_cols: [integer]}
  # a felsorolt koordinátájú sátrak másikat érintenek
  @type err_touch :: %{err_touch: [field]}
  # hibaleíró hármas
  @type errs_desc :: {err_rows, err_cols, err_touch}

  @spec check_sol(pd :: puzzle_desc, ds :: tent_dirs) :: ed :: errs_desc
  # Az {rs, cs, ts} = pd feladványleíró és a ds sátorirány-lista
  # alapján elvégzett ellenőrzés eredménye a ed hibaleíró, ahol
  #   rs a sátrak soronként elvárt számának a listája,
  #   cs a sátrak oszloponként elvárt számának a listája,
  #   ts a fákat tartalmazó parcellák koordinátájának a listája
  # Az {e_rows, e_cols, e_touch} = ed hármas elemei olyan
  # kulcs-érték párok, melyekben a kulcs a hiba jellegére utal, az
  # érték pedig a hibahelyeket felsoroló lista (üres, ha nincs hiba)

  def check_sol({tents_count_rows, tents_count_cols, trees}, ds) do
    tents =
      Enum.sort(
        for {{row, col}, dir} <- List.zip([trees, ds]) do
          case dir do
            :n -> {row - 1, col}
            :e -> {row, col + 1}
            :s -> {row + 1, col}
            :w -> {row, col - 1}
          end
        end,
        fn
          {row, col1}, {row, col2} -> col1 <= col2
          {row1, _}, {row2, _} -> row1 <= row2
        end
      )

    err_rows =
      for {count, index} <- Enum.with_index(tents_count_rows, 1),
          count >= 0,
          count !==
            List.foldl(
              tents,
              0,
              fn
                {^index, _}, acc -> acc + 1
                {_, _}, acc -> acc
              end
            ),
          do: index

    err_cols =
      for {count, index} <- Enum.with_index(tents_count_cols, 1),
          count >= 0,
          count !==
            List.foldl(
              tents,
              0,
              fn
                {_, ^index}, acc -> acc + 1
                {_, _}, acc -> acc
              end
            ),
          do: index

    set = List.foldl(tents, MapSet.new(), &(&2 |> MapSet.put(&1)))

    err_touch =
      for {row, col} <- MapSet.to_list(set),
          MapSet.member?(set, {row - 1, col - 1}) or
            MapSet.member?(set, {row - 1, col}) or
            MapSet.member?(set, {row - 1, col + 1}) or
            MapSet.member?(set, {row, col + 1}) or
            MapSet.member?(set, {row + 1, col + 1}) or
            MapSet.member?(set, {row + 1, col}) or
            MapSet.member?(set, {row + 1, col - 1}) or
            MapSet.member?(set, {row, col - 1}),
          do: {row, col}

    {%{err_rows: err_rows}, %{err_cols: err_cols}, %{err_touch: err_touch}}
  end
end
