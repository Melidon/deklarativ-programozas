defmodule Khf1 do
  @moduledoc """
  Kemping
  @author "Szommer Zsombor <zsombor.szommer@protonmail.com>"
  @date   "2022-09-25"
  ...
  """

  @doc """
  ...
  """
  # sor száma (1-től n-ig)
  @type row :: integer
  # oszlop száma (1-től m-ig)
  @type col :: integer
  # egy parcella koordinátái
  @type field :: {row, col}

  # a sátrak száma soronként
  @type tentsCountRows :: [integer]
  # a sátrak száma oszloponként
  @type tentsCountCols :: [integer]

  # a fákat tartalmazó parcellák koordinátái
  @type trees :: [field]
  # a feladványleíró hármas
  @type puzzle_desc :: {tentsCountRows, tentsCountCols, trees}

  @doc """
  ...
  """
  @spec to_internal(file :: String.t()) :: pd :: puzzle_desc
  # A file fájlban szövegesen ábrázolt feladvány leírója pd

  def to_internal(file) do
    [first_row | rows] = for row <- String.split(File.read!(file), "\n"), String.trim(row) !== "", do: row
    tcr = for n <- String.split(first_row, ~r{\s}, trim: true), do: String.to_integer(n)
    {tcc, matrix} = Enum.unzip(for row <- rows, [first_item | items] = String.split(row, ~r{\s}, trim: true), do: {String.to_integer(first_item), items})
    cords = for {row, i} <- Enum.with_index(matrix), {x, j} <- Enum.with_index(row), x === "*", do: {i+1, j+1}
    {tcc, tcr, cords}
  end
end
