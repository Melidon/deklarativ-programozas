% seq(+N, +M, -L): Az L lista M-N+1 hosszú, elemei 1 különbségû számtani
% sorozatot alkotnak, és L első eleme (ha van) N, ahol N és M egész számok.
seq(N, N, [N]).
seq(N, M, [N | L]) :- N < M, N1 is N+1, seq(N1, M, L).

% max(+N, ?X): X egy egész szám, melyre 0 < X =< N, ahol N adott
% egész szám. Az eljárás a fenti feltételeknek megfelelő X számokat
% sorolja fel. A felsorolás sorrendjére nem teszünk megkötést.
max(N, X) :- 0 < N, X is N.
max(N, X) :- 0 < N, N1 is N-1, max(N1, X).

% hatv(+A, +E, -H):  H = A ^ E, ahol A egész szám, E >= 0 egész szám.
hatv(_A, 0, 1).
hatv(A, E, H) :- 0 < E, E1 is E-1, hatv(A, E1, H1), H is A*H1.

% fa_pontszama(*Fa, -N): A Fa bináris fa csomópontjainak száma N.
fa_pontszama(leaf(_), 0).
fa_pontszama(node(BAL, JOBB), N) :- fa_pontszama(BAL, N1), fa_pontszama(JOBB, N2), N is 1+N1+N2.

% fa_noveltje(*Fa0, ?Fa): Fa úgy áll elő a Fa0 bináris fából, hogy az
% utóbbi minden egyes levelében levő értéket 1-gyel megnöveljük.
fa_noveltje(leaf(X), leaf(X1)) :- X1 is X+1.
fa_noveltje(node(Bal, Jobb), node(Bal1, Jobb1)) :- fa_noveltje(Bal, Bal1), fa_noveltje(Jobb, Jobb1).

% lista_hossza(*Lista, -Hossz): A Lista egészlista hossza Hossz.
lista_hossza([], 0).
lista_hossza([_ | XS], H) :- lista_hossza(XS, H1), H is H1 + 1.

% lista_hossza2(*Lista, -Hossz): A Lista egészlista hossza Hossz.    
% Jobbrekurzív változat
seged([], A, A).
seged([_|XS], A, H) :- A1 is A + 1, seged(XS, A1, H).
lista_hossza2(XS, H) :- seged(XS, 0, H).

% lista_noveltje(*L0, ?L): Az L egészlista úgy áll elő az L0
% egészlistából, hogy az utóbbi minden egyes elemét 1-gyel megnöveljük.
lista_noveltje([], []).
lista_noveltje([X|XS], L) :- X1 is X + 1, lista_noveltje(XS, L1), L = [X1|L1].

% lista_utolso_eleme(*L, ?Ertek): Az L egészlista utolsó eleme Ertek.
lista_utolso_eleme([X], X).
lista_utolso_eleme([_|XS], X) :- lista_utolso_eleme(XS, X).

% fa_levelerteke(*Fa, -Ertek): A Fa bináris fa egy levelében található
% érték az Ertek.
fa_levelerteke(leaf(E), E).
fa_levelerteke(node(BAL, JOBB), E) :- fa_levelerteke(BAL, E); fa_levelerteke(JOBB, E).

% fa_reszfaja(*Fa, -Resz): Resz a Fa bináris fa részfája.
fa_reszfaja(E, E).
fa_reszfaja(node(BAL, JOBB), E) :- fa_reszfaja(BAL, E); fa_reszfaja(JOBB, E).

% lista_prefixuma(*L0, -L): L az L0 egészlista prefixuma.
lista_prefixuma([X|XS], SZ) :- lista_prefixuma(XS, SZ1), SZ = [X|SZ1].
lista_prefixuma(_, []).
