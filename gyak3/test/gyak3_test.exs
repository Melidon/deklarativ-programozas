defmodule Gyak3Test do
  use ExUnit.Case

  test "fa_noveltje test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}

    assert Gyak3.fa_noveltje(t1) ===
             {5, {4, :level, :level}, {7, {6, :level, :level}, {8, :level, :level}}}
  end

  test "fa_tukorkepe test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}

    assert Gyak3.fa_tukorkepe(t1) ===
             {4, {6, {7, :level, :level}, {5, :level, :level}}, {3, :level, :level}}
  end

  test "order, preorder and postorder" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}
    assert Gyak3.inorder(t1) === [3, 4, 5, 6, 7]
    assert Gyak3.preorder(t1) === [4, 3, 6, 5, 7]
    assert Gyak3.postorder(t1) === [3, 5, 7, 6, 4]
  end

  test "tartalmaz test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}

    t2 =
      {:a, {:b, {:v, :level, :level}, :level},
       {:c, :level,
        {:d, {:w, {:x, :level, :level}, :level}, {:f, {:x, :level, :level}, {:y, :level, :level}}}}}

    assert Gyak3.tartalmaz(t1, :x) === false
    assert Gyak3.tartalmaz(t2, :x) === true
  end

  test "elofordul test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}

    t2 =
      {:a, {:b, {:v, :level, :level}, :level},
       {:c, :level,
        {:d, {:w, {:x, :level, :level}, :level}, {:f, {:x, :level, :level}, {:y, :level, :level}}}}}

    assert Gyak3.elofordul(t1, :x) === 0
    assert Gyak3.elofordul(t2, :x) === 2
  end

  test "cimkek test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}
    assert Gyak3.cimkek(t1) === [3, 4, 5, 6, 7]
  end

  test "fa_balerteke test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}
    assert Gyak3.fa_balerteke(t1) === {:ok, 3}
    assert Gyak3.fa_balerteke(:level) === :error
  end

  test "fa_jobberteke test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}
    assert Gyak3.fa_jobberteke(t1) === {:ok, 7}
    assert Gyak3.fa_jobberteke(:level) === :error
  end

  test "rendezett_fa test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}

    t2 =
      {:a, {:b, {:v, :level, :level}, :level},
       {:c, :level,
        {:d, {:w, {:x, :level, :level}, :level}, {:f, {:x, :level, :level}, {:y, :level, :level}}}}}

    assert Gyak3.rendezett_fa(t1) === true
    assert Gyak3.rendezett_fa(t2) === false
  end

  test "utak test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}

    t2 =
      {:a, {:b, {:v, :level, :level}, :level},
       {:c, :level,
        {:d, {:w, {:x, :level, :level}, :level}, {:f, {:x, :level, :level}, {:y, :level, :level}}}}}

    assert Gyak3.utak(t1) === [{4, []}, {3, [4]}, {6, [4]}, {5, [4, 6]}, {7, [4, 6]}]

    assert Gyak3.utak(t2) === [
             {:a, []},
             {:b, [:a]},
             {:v, [:a, :b]},
             {:c, [:a]},
             {:d, [:a, :c]},
             {:w, [:a, :c, :d]},
             {:x, [:a, :c, :d, :w]},
             {:f, [:a, :c, :d]},
             {:x, [:a, :c, :d, :f]},
             {:y, [:a, :c, :d, :f]}
           ]
  end

  test "cutak test" do
    t1 = {4, {3, :level, :level}, {6, {5, :level, :level}, {7, :level, :level}}}

    t2 =
      {:a, {:b, {:v, :level, :level}, :level},
       {:c, :level,
        {:d, {:w, {:x, :level, :level}, :level}, {:f, {:x, :level, :level}, {:y, :level, :level}}}}}

    assert Gyak3.cutak(t1, :x) === []
    assert Gyak3.cutak(t2, :x) === [{:x, [:a, :c, :d, :w]}, {:x, [:a, :c, :d, :f]}]
  end
end
