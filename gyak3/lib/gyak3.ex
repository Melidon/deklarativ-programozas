defmodule Gyak3 do
  @moduledoc """
  Documentation for `Gyak3`.
  """
  @type fa :: :level | {any, fa, fa}
  @type egeszfa :: :level | {integer, egeszfa, egeszfa}

  @spec fa_noveltje(f0 :: egeszfa) :: f :: egeszfa
  # Az f fa minden címkéje eggyel nagyobb az f0 fa azonos helyen lévő címkéjénél
  def fa_noveltje(:level), do: :level

  def fa_noveltje({i, bal, jobb}) do
    {i + 1, fa_noveltje(bal), fa_noveltje(jobb)}
  end

  @spec fa_tukorkepe(f0 :: fa) :: f :: fa
  # f az f0 fa tükörképe
  def fa_tukorkepe(:level), do: :level

  def fa_tukorkepe({i, bal, jobb}) do
    {i, fa_tukorkepe(jobb), fa_tukorkepe(bal)}
  end

  @spec inorder(f :: fa) :: ls :: [any]
  # ls az f fa elemeinek a fa inorder bejárásával létrejövő listája
  @spec preorder(f :: fa) :: ls :: [any]
  # ls az f fa elemeinek a fa preorder bejárásával létrejövő listája
  @spec postorder(f :: fa) :: ls :: [any]
  # ls az f fa elemeinek a fa postorder bejárásával létrejövő listája
  def inorder(:level), do: []

  def inorder({i, bal, jobb}) do
    inorder(bal) ++ [i] ++ inorder(jobb)
  end

  def preorder(:level), do: []

  def preorder({i, bal, jobb}) do
    [i] ++ preorder(bal) ++ preorder(jobb)
  end

  def postorder(:level), do: []

  def postorder({i, bal, jobb}) do
    postorder(bal) ++ postorder(jobb) ++ [i]
  end

  @spec tartalmaz(f :: fa, c :: any) :: b :: boolean
  # b igaz, ha c az f fa valamely címkéje
  def tartalmaz(:level, _), do: false

  def tartalmaz({i, bal, jobb}, c) do
    i === c or tartalmaz(bal, c) or tartalmaz(jobb, c)
  end

  @spec elofordul(f :: fa, c :: any) :: n :: integer
  # A c címke az f fában n-szer fordul elő
  def elofordul(:level, _), do: 0

  def elofordul({i, bal, jobb}, c) do
    if(i === c, do: 1, else: 0) + elofordul(bal, c) + elofordul(jobb, c)
  end

  @spec cimkek(f :: fa, zs :: [any]) :: ls :: [any]
  # ls az f címkéinek listája inorder sorrendben zs elé fűzve
  def cimkek({i, bal, jobb}, zs) do
    cimkek(bal, [i | cimkek(jobb, zs)])
  end

  def cimkek(:level, zs), do: zs
  @spec cimkek(f :: fa) :: ls :: [any]
  # ls az f címkéinek listája inorder sorrendben
  def cimkek(f) do
    cimkek(f, [])
  end

  @spec fa_balerteke(f :: fa) :: {:ok, c :: any} | :error
  # Egy nemüres f fa bal oldali szélső címkéje c (minden
  # felmenőjére is igaz, hogy bal oldali gyermek)
  def fa_balerteke(:level), do: :error
  def fa_balerteke({i, :level, _}), do: {:ok, i}
  def fa_balerteke({_, bal, _}), do: fa_balerteke(bal)

  @spec fa_jobberteke(f :: fa) :: {:ok, c :: any} | :error
  # Egy nemüres f fa jobb oldali szélső címkéje c (minden
  # felmenőjére is igaz, hogy jobb oldali gyermek)
  def fa_jobberteke(:level), do: :error
  def fa_jobberteke({i, _, :level}), do: {:ok, i}
  def fa_jobberteke({_, _, jobb}), do: fa_jobberteke(jobb)

  @spec rendezett_fa(f :: fa) :: b :: boolean
  # b igaz, ha az f fa rendezett
  def rendezett_fa(f) do
    [x | xs] = cimkek(f)
    {_, res} = List.foldl(xs, {x, true}, fn x, {p, b} -> {x, b and x > p} end)
    res
  end

  @type ut :: [any]
  @spec utak(f :: fa, eddigi :: ut) :: cimkezett_utak :: [{c :: any, u :: ut}]
  # A cimkezett_utak lista az f fa minden csomópontjához egy {c,u} párt
  # társít, ahol c az adott csomópont címkéje, u pedig az adott
  # csomóponthoz vezető útvonal az eddigi eddigi útvonal elé fűzve
  def utak({i, bal, jobb}, ut) do
    ujut = ut ++ [i]
    [{i, ut}] ++ utak(bal, ujut) ++ utak(jobb, ujut)
  end

  def utak(:level, _), do: []

  @spec utak(f :: fa) :: cimkezett_utak :: [{c :: any, cu :: ut}]
  # A cimkezett_utak lista az f fa minden csomópontjához egy {c,cu} párt
  # társít, ahol c az adott csomópont címkéje, cu pedig az adott
  # csomóponthoz vezető útvonal
  def utak(f), do: utak(f, [])

  @spec cutak(f :: fa, c :: any) :: utak :: [{c :: any, cu :: ut}]
  # utak azon csomópontok útvonalainak listája f-ben, amelyek címkéje c
  def cutak(f, c) do
    Enum.filter(utak(f), fn {x, _} -> x === c end)
  end
end
