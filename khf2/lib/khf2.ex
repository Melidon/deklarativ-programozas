defmodule Khf2 do
  @moduledoc """
  Kemping
  @author "Szommer Zsombor <zsombor.szommer@protonmail.com>"
  @date   "2022-10-01"
  """

  # sor száma (1-től n-ig)
  @type row :: integer
  # oszlop száma (1-től m-ig)
  @type col :: integer
  # egy parcella koordinátái
  @type field :: {row, col}

  # a sátrak száma soronként
  @type tents_count_rows :: [integer]
  # a sátrak száma oszloponként
  @type tents_count_cols :: [integer]

  # a fákat tartalmazó parcellák koordinátái lexikálisan rendezve
  @type trees :: [field]
  # a feladványleíró hármas
  @type puzzle_desc :: {tents_count_rows, tents_count_cols, trees}

  # a sátorpozíciók iránya: north, east, south, west
  @type dir :: :n | :e | :s | :w
  # a sátorpozíciók irányának listája a fákhoz képest
  @type tent_dirs :: [dir]

  @spec to_external(pd :: puzzle_desc, ds :: tent_dirs, file :: String.t()) :: :ok
  # A pd = {rs, cs, ts} feladványleíró és a ds sátorirány-lista alapján
  # a feladvány szöveges ábrázolását írja ki a file fájlba, ahol
  #   rs a sátrak soronkénti számának a listája,
  #   cs a sátrak oszloponkénti számának a listája,
  #   ts a fákat tartalmazó parcellák koordinátájának lexikálisan rendezett listája

  def to_external({tents_count_rows, tents_count_cols, trees}, ds, file) do
    map =
      List.foldl(List.zip([trees, ds]), Map.new(), fn {{row, col}, dir}, map ->
        case dir do
          :n -> map |> Map.put({row - 1, col}, "N")
          :e -> map |> Map.put({row, col + 1}, "E")
          :s -> map |> Map.put({row + 1, col}, "S")
          :w -> map |> Map.put({row, col - 1}, "W")
        end
        |> Map.put({row, col}, "*")
      end)

    matrix =
      for row <- 1..length(tents_count_rows),
          do: for(col <- 1..length(tents_count_cols), do: map |> Map.get({row, col}, "-"))

    pad_count = 2
    divider = " "

    first_row =
      List.foldl(
        tents_count_cols,
        "" |> String.pad_leading(pad_count),
        &(&2 <> divider <> (&1 |> Integer.to_string() |> String.pad_leading(pad_count)))
      )

    rows =
      for {tents_count_row, row} <- List.zip([tents_count_rows, matrix]),
          do:
            List.foldl(
              row,
              tents_count_row |> Integer.to_string() |> String.pad_leading(pad_count),
              &(&2 <> divider <> (&1 |> String.pad_leading(pad_count)))
            )

    output = List.foldl(rows, first_row, &(&2 <> "\n" <> &1))

    File.write!(file, output)

    :ok
  end
end
