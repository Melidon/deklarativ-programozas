% insert_ord(+RL0, +Elem, ?RL): Az RL szigorúan monoton növõ számlista
% úgy áll elõ, hogy az RL0 szigorúan növõ számlistába beszúrjuk az Elem
% számot, feltéve hogy Elem nem eleme az RL0 listának; egyébként RL = RL0.
insert_ord([], Elem, [Elem]).
insert_ord([Fej|RL0], Elem, RL) :-
    (   Fej < Elem, insert_ord(RL0, Elem, RR), RL = [Fej|RR]
    ;   Fej = Elem, RL = [Fej|RL0]
    ;   Fej > Elem, RL = [Elem, Fej|RL0]
    ).

house([a-b,a-c,b-c,b-d,b-e,c-d,c-e,d-e]).

% graph(G): G egy 'graph' típusba tartozó Prolog kifejezés.
edge(Node1-Node2) :-
    atom(Node1),
    atom(Node2).
graph([]).
graph([Edge|Edges]) :-
    edge(Edge),
    graph(Edges).

same_edge(A-B, A-B).
same_edge(A-B, B-A).

% same_graph0(G1, G2): G1 és G2 azonos gráfot írnak le.
same_graph([], []).
same_graph([G|G1], G2) :-
	same_edge(G, Edge),
    same_length([G|G1], G2),
	select(Edge, G2, UjG2),
	same_graph(G1, UjG2).

% line(G, P):  A G gráf a P pontból kiinduló folytonos vonal.
line([], _).
line([A-B|Graph], A) :-
    line(Graph, B).
