fabol_es_iranybol_sator(ROW-COL, n, ROW1-COL) :- ROW1 is ROW-1.
fabol_es_iranybol_sator(ROW-COL, e, ROW-COL1) :- COL1 is COL+1.
fabol_es_iranybol_sator(ROW-COL, s, ROW1-COL) :- ROW1 is ROW+1.
fabol_es_iranybol_sator(ROW-COL, w, ROW-COL1) :- COL1 is COL-1.

fa_es_irany_listakbol_sator_lista([], _, []).
fa_es_irany_listakbol_sator_lista([FA|FAK], [IRANY|IRANYOK], [SATOR|SATRAK]) :-
                                  fabol_es_iranybol_sator(FA, IRANY, SATOR),
                                  fa_es_irany_listakbol_sator_lista(FAK, IRANYOK, SATRAK).

build_matrix(ActualRow-_, MaxRow-_, _, []) :-
    ActualRow > MaxRow.
build_matrix(ActualRow-ActualCol, MaxRow-MaxCol, Tents, [[]|Result]) :-
    ActualCol > MaxCol,
    NextRow is ActualRow + 1,
    NextCol is 1,
    build_matrix(NextRow-NextCol, MaxRow-MaxCol, Tents, Result).
build_matrix(ActualRow-ActualCol, MaxRow-MaxCol, [ActualRow-ActualCol|Tents], Result) :-
    NextCol is ActualCol + 1,
    build_matrix(ActualRow-NextCol, MaxRow-MaxCol, Tents, [Row|Rows]),
    Result = [[1|Row]|Rows],
	!.
build_matrix(ActualRow-ActualCol, MaxRow-MaxCol, Tents, Result) :-
    NextCol is ActualCol + 1,
    build_matrix(ActualRow-NextCol, MaxRow-MaxCol, Tents, [Row|Rows]),
    Result = [[0|Row]|Rows],
    !.

check_out_of_map(_, []).
check_out_of_map(MaxRow-MaxCol, [Row-Col|Tents]) :-
    1 =< Row, Row =< MaxRow,
    1 =< Col, Col =< MaxCol,
    check_out_of_map(MaxRow-MaxCol, Tents).

satrak_mx(NM, Fs, Ss, Mx) :-
    fa_es_irany_listakbol_sator_lista(Fs, Ss, Tents),
    check_out_of_map(NM, SortedTents),
    sort(Tents, SortedTents),
    length(Tents, L1),
    length(SortedTents, L2),
    L1 = L2,
    build_matrix(1-1, NM, SortedTents, Mx).
